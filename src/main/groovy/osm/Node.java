package osm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import osm.NodeDegreeData.Type;
import osm.cleaner.Baton;

@XmlRootElement(name = "node")
public class Node implements Comparable<Node> {
	public static final Node EMPTY = new Node("EMPTY", "0", "0");
	public static final int NOT_IN_GRID = -1;
	private String id;
	private String visible;
	private String lat;
	private String lon;
	private ArrayList<Tag> tagList;
	private NodeDegreeData data;
	private int latGridNo;
	private int lonGridNo;
	private int indegree;
	private int outdegree;

	public Node() {
		this("", "", "", new ArrayList<Tag>());
	}

	// For equals
	public Node(String id) {
		this(id, "", "", null);
	}

	public Node(String id, String lat, String lon) {
		this(id, lat, lon, new ArrayList<Tag>());
	}

	public Node(String id, String lat, String lon, ArrayList<Tag> tagList) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		latGridNo = NOT_IN_GRID;
		lonGridNo = NOT_IN_GRID;
		this.tagList = tagList;
		visible = "true";
	}

	public boolean isInGrid() {
		return latGridNo >= 0 || lonGridNo >= 0;
	}

	public String getName() {
		String name = "";
		for (Tag t : tagList) {
			if ("name".equals(t.getK())) {
				name = t.getV();
			}
		}
		return name;
	}

	public Nd toNd() {
		return new Nd(id);
	}

	@XmlAttribute
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlAttribute
	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	@XmlAttribute
	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	@XmlAttribute
	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	@XmlElement(name = "tag", required = false)
	public ArrayList<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(ArrayList<Tag> tagList) {
		this.tagList = tagList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(id).append(",").append(lat).append(",").append(lon);
		return builder.toString();
	}

	public void setLatGridNo(int latGridNo) {
		this.latGridNo = latGridNo;
	}

	public void setLonGridNo(int lonGridNo) {
		this.lonGridNo = lonGridNo;
	}

	public int getLatGridNo() {
		return latGridNo;
	}

	public int getLonGridNo() {
		return lonGridNo;
	}

	@Override
	public int compareTo(Node o) {
		return id.compareTo(o.getId());
	}

	public void setType(Type type) {
		lazyInitializeData();
		data.setType(type);
	}

	public Type getType() {
		lazyInitializeData();
		return data.getType();
	}

	// TODO: Add guard against duplicate entries
	public void addOutNode(Node outNode) {
		lazyInitializeData();
		data.addOutNode(outNode);
	}

	// TODO: Add guard against duplicate entries
	public void addInNode(Node inNode) {
		lazyInitializeData();
		data.addInNode(inNode);
	}

	public List<Node> getOutNodes() {
		lazyInitializeData();
		return data.getOutNodes();
	}

	public List<Node> getInNodes() {
		lazyInitializeData();
		return data.getInNodes();
	}

	public int getDegree() {
		lazyInitializeData();
		return data.getDegree();
	}

	public List<Baton> getOutBatons() {
		lazyInitializeData();
		return data.getOutBatons();
	}

	public List<Baton> getInBatons() {
		lazyInitializeData();
		return data.getInBatons();
	}

	public void addOutBaton(Baton baton) {
		lazyInitializeData();
		data.addOutBaton(baton);
	}

	public void addInBaton(Baton baton) {
		lazyInitializeData();
		data.addInBaton(baton);
	}

	public void removeOutNode(Node node) {
		data.removeOutNode(node);
	}

	public void removeOutBaton(Baton baton) {
		data.removeOutBaton(baton);
	}

	public void removeInNode(Node node) {
		data.removeInNode(node);
	}

	public void removeInBaton(Baton baton) {
		data.removeInBaton(baton);
	}

	private void lazyInitializeData() {
		if (data == null) {
			data = new NodeDegreeData();
		}
	}

	public boolean isEmpty() {
		return this.equals(EMPTY);
	}

	public int getIndegree() {
		return indegree;
	}

	public void setIndegree(int indegree) {
		this.indegree = indegree;
	}
	
	public void incIndegree() {
		indegree++;
	}

	public int getOutdegree() {
		return outdegree;
	}

	public void setOutdegree(int outdegree) {
		this.outdegree = outdegree;
	}
	
	public void incOutdegree() {
		outdegree++;
	}

}
