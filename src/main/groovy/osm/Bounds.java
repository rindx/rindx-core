package osm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "bounds")
public class Bounds {
	private String minlat;
	private String minlon;
	private String maxlat;
	private String maxlon;

	public Bounds() {
	}

	public Bounds(String minlat, String minlon, String maxlat, String maxlon) {
		this.minlat = minlat;
		this.minlon = minlon;
		this.maxlat = maxlat;
		this.maxlon = maxlon;
	}

	@XmlAttribute
	public String getMinlat() {
		return minlat;
	}

	public void setMinlat(String minlat) {
		this.minlat = minlat;
	}

	@XmlAttribute
	public String getMinlon() {
		return minlon;
	}

	public void setMinlon(String minlon) {
		this.minlon = minlon;
	}

	@XmlAttribute
	public String getMaxlat() {
		return maxlat;
	}

	public void setMaxlat(String maxlat) {
		this.maxlat = maxlat;
	}

	@XmlAttribute
	public String getMaxlon() {
		return maxlon;
	}

	public void setMaxlon(String maxlon) {
		this.maxlon = maxlon;
	}

}
