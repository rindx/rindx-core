package osm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import osm.cleaner.RoadLink;
import osm.cleaner.RoadLink.Highway;

public class FileUtils {
	public static List<Node> readNodes(String nodesFilename) throws FileNotFoundException {
		File nodesFile = new File(nodesFilename);
		List<Node> nodes = new ArrayList<>();
		try (Scanner scanner = new Scanner(nodesFile)) {
			scanner.nextLine();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] values = line.split("\t");
				Node node = new Node(values[0], values[1], values[2]);
				nodes.add(node);
			}
		}
		return nodes;
	}
	
	public static List<RoadLink> readRoadlinks(String roadLinksFilename) throws FileNotFoundException {
		File roadLinksFile = new File(roadLinksFilename);
		List<RoadLink> roadLinks = new ArrayList<>();
		try (Scanner scanner = new Scanner(roadLinksFile)) {
			scanner.nextLine();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] values = line.split(";");
				Node start = new Node(values[0]);
				Node end = new Node(values[1]);
				if (values.length == 6) {
					BigDecimal distance = new BigDecimal(
							values[values.length - 3]);
					int lanes = Integer.parseInt(values[values.length - 2]);
					Highway highwayType = getHighwayType(values[values.length - 1]);
					RoadLink roadLink = new RoadLink(start, end, values[2],
							distance, lanes, highwayType);
					roadLinks.add(roadLink);
				} else if (values.length == 5) {
					BigDecimal distance = new BigDecimal(
							values[values.length - 2]);
					int lanes = Integer.parseInt(values[values.length - 1]);
					RoadLink roadLink = new RoadLink(start, end, values[2],
							distance, lanes);
					roadLinks.add(roadLink);
				}
			}
		}
		return roadLinks;
	}
	
	private static Highway getHighwayType(String highwayType) {
		switch (highwayType.toLowerCase()) {
		case "tertiary":
			return Highway.TERTIARY;
		case "tertiary_link":
			return Highway.TERTIARY_LINK;
		case "secondary":
			return Highway.SECONDARY;
		case "secondary_link":
			return Highway.SECONDARY_LINK;
		case "primary":
			return Highway.PRIMARY;
		case "primary_link":
			return Highway.PRIMARY_LINK;
		case "trunk":
			return Highway.TRUNK;
		case "trunk_link":
			return Highway.TRUNK_LINK;
		case "motorway":
			return Highway.MOTORWAY;
		case "motorway_link":
			return Highway.MOTORWAY_LINK;
		case "service":
			return Highway.SERVICE;
		case "unclassified":
			return Highway.UNCLASSIFIED;
		}
		return null;
	}
	
	public static String readFileToString(String filename) throws IOException {
		return new String(Files.readAllBytes(Paths.get(filename)));
	}
	
	public static File forceMkdirParent(String path) {
		return forceMkdirParent(new File(path));
	}

	private static File forceMkdirParent(File file) {
		if (file.getParent() != null) {
			File parent = new File(file.getParent());
			parent.mkdirs();
		}
		return file;
	}

	public static PrintWriter createPrintWriter(File file) throws IOException {
		return new PrintWriter(new BufferedWriter(new FileWriter(file)));
	}

}
