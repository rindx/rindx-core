package osm;

public class CaseParams {
	private String experimentId;
	private String radius;
	private String targetThreshold;
	private String centerId;
	private String lat;
	private String lon;

	public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getTargetThreshold() {
		return targetThreshold;
	}

	public void setTargetThreshold(String targetThreshold) {
		this.targetThreshold = targetThreshold;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}
}
