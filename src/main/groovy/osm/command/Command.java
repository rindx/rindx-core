package osm.command;

public interface Command {
	void execute();
}
