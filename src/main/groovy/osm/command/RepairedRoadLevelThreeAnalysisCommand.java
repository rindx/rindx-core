package osm.command;

import osm.Analysis;
import osm.RepairedRoadsAnalyzer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static osm.FileUtils.createPrintWriter;
import static osm.FileUtils.forceMkdirParent;
import static osm.FileUtils.readFileToString;

public class RepairedRoadLevelThreeAnalysisCommand implements Command {
    private String repairedRoadsFilename;
    private String outputFilename;
    private RepairedRoadsAnalyzer analyzer;

    public RepairedRoadLevelThreeAnalysisCommand(String repairedRoadsFilename,
                                                 String outputFilename) {
        this.repairedRoadsFilename = repairedRoadsFilename;
        this.outputFilename = outputFilename;
        analyzer = new RepairedRoadsAnalyzer();
    }

    @Override
    public void execute() {
        try {
            String content = readFileToString(repairedRoadsFilename);
            List<Analysis> analyses = analyzer.analyze(content);
            writeLevelThreeAnalysisToFile(outputFilename, analyses);
        } catch (IOException e) {
            throw new RepairAnalysisCommandException(e);
        }
    }

    private void writeLevelThreeAnalysisToFile(String filename,
                                               List<Analysis> analyses) {
        try (PrintWriter writer = createPrintWriter(forceMkdirParent(filename))) {
            for (Analysis analysis : analyses) {
                writer.println(analysis.printLevelThreeTypeInfo());
            }
        } catch (IOException e) {
            throw new RepairAnalysisCommandException("Error Writing to File: "
                    + filename, e);
        }
    }

    public String getRepairedRoadsFilename() {
        return repairedRoadsFilename;
    }

    public String getOutputFilename() {
        return outputFilename;
    }
}