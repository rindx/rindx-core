package osm.cleaner;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import osm.Node;

public class Component implements Comparable<Component>, Iterable<Node> {
	private List<Node> nodes;
	private List<RoadLink> successorEdges;
	private int index;
	private int traverseIndex;
	private boolean inStack;

	public Component(List<Node> nodes) {
		this.nodes = nodes;
		index = -1;
		traverseIndex = -1;
		inStack = false;
	}

	public boolean isUnvisited() {
		return traverseIndex == -1;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void add(Node node) {
		nodes.add(node);
	}

	public int size() {
		return nodes.size();
	}

	@Override
	public int compareTo(Component o) {
		return ((Integer) this.size()).compareTo(o.size());
	}

	@Override
	public Iterator<Node> iterator() {
		return nodes.iterator();
	}

	public Node get(int index) {
		return nodes.get(index);
	}

	public boolean containsAll(List<Node> c) {
		return nodes.containsAll(c);
	}

	public boolean contains(Node n) {
		return nodes.contains(n);
	}

	public List<RoadLink> getSuccessorEdges() {
		if (successorEdges == null) {
			successorEdges = new LinkedList<>();
		}
		return successorEdges;
	}

	public void setSuccessorEdges(List<RoadLink> successorEdges) {
		this.successorEdges = successorEdges;
	}

	public void addSuccessorEdge(RoadLink successorEdge) {
		if (successorEdges == null) {
			successorEdges = new LinkedList<>();
		} else if (!successorEdges.contains(successorEdge)) {
			successorEdges.add(successorEdge);
		}
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public void setTraverseIndex(int traverseIndex) {
		this.traverseIndex = traverseIndex;
	}

	public boolean isInStack() {
		return inStack;
	}

	public void setInStack(boolean inStack) {
		this.inStack = inStack;
	}
}
