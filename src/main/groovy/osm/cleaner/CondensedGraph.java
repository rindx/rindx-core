package osm.cleaner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import osm.Node;

public class CondensedGraph {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CondensedGraph.class);
	private List<Component> components;
	private List<RoadLink> roadLinks;
	private boolean isStale;
	private short[][] matrix;
	private int index;
	private Deque<Component> stack;
	private Map<Node, Integer> nodeMap;
	private final long MAX_REACHABILITY;
	private double reachability;

	// TODO: Consider creating a constructor that will accept a graph and
	// condense it?
	public CondensedGraph(List<Component> components, List<RoadLink> roadLinks) {
		this.components = components;
		initializeComponentIndices(components);
		this.roadLinks = roadLinks;
		nodeMap = createComponentMap(components);
		isStale = true;
		MAX_REACHABILITY = getDenominator();
		reachability = -1;
	}

	private long getDenominator() {
		long rowSum = 0;
		for (Component component : components) {
			rowSum += component.size();
		}
		return rowSum * rowSum;
	}

	private void initializeComponentIndices(List<Component> components) {
		for (int i = 0; i < components.size(); i++) {
			components.get(i).setIndex(i);
		}
	}

	public List<Component> getComponents() {
		return components;
	}

	public List<RoadLink> getRoadLinks() {
		return roadLinks;
	}

	public void addRoad(Road road) {
		for (RoadLink roadLink : road.getRoadLinks()) {
			addRoadLink(roadLink);
		}
	}

	public void addRoadLink(RoadLink roadLink) {
		if (roadLink == null) {
			throw new ExperimentException("Null Roadlink");
		}
		roadLinks.add(roadLink);
		isStale = true;
	}

	public int getNumNodes() {
		return components.size();
	}

	public int getNumEdges() {
		return roadLinks.size();
	}

	// TODO: removeInteriorRoads(). Speed boost
	public List<Road> repairPriority(List<Road> brokenRoads) {
		List<Road> localBrokenRoads = new ArrayList<>(brokenRoads);
		Collections.sort(localBrokenRoads);
		List<Road> repairPriority = new LinkedList<>();
		int counter = 0;
		int total = localBrokenRoads.size();
		double connectivity = connectivity();
		List<Road> purge = new ArrayList<>();
		while (connectivity < 1) {
			Road maxEdge = localBrokenRoads.get(0);
			double maxRepairImpact = repairImpactOf(maxEdge);
			int maxEdgeIndex = 0;
			for (int i = 1; i < localBrokenRoads.size(); i++) {
				Road edge = localBrokenRoads.get(i);
				double repairImpact = repairImpactOf(edge);
				if (repairImpact > maxRepairImpact) {
					maxRepairImpact = repairImpact;
					maxEdge = edge;
					maxEdgeIndex = i;
				} else if (repairImpact == connectivity) {
					purge.add(edge);
				}
			}
			addRoad(maxEdge);
			updateComponents();
			localBrokenRoads.remove(maxEdgeIndex);
			purgeRoads(localBrokenRoads, purge);
			isStale = true;
			repairPriority.add(maxEdge);
			double difference = maxRepairImpact - connectivity;
			connectivity = maxRepairImpact;
			counter++;
			LOGGER.info("Repaired {}/{} - {} , +{}", counter, total,
					connectivity, difference);
		}

		return repairPriority;
	}

	private void purgeRoads(List<Road> brokenRoads, List<Road> purge) {
		for (Road road : purge) {
			int index = Collections.binarySearch(brokenRoads, road);
			if (index < 0) {
				throw new IndexOutOfBoundsException(
						"index should not never be negative");
			}
			brokenRoads.remove(index);
		}
		purge.clear();
	}

	private void updateComponents() {
		isStale = true;
	}

	private Map<Node, Integer> createComponentMap(List<Component> comps) {
		Map<Node, Integer> map = new HashMap<>();
		for (int i = 0; i < comps.size(); i++) {
			Component c = comps.get(i);
			for (Node n : c.getNodes()) {
				map.put(n, i);
			}
		}
		return map;
	}

	protected double repairImpactOf(Road edge) {
		short[][] futureMatrix = copyPathMatrix();
		for (RoadLink link : edge.getRoadLinks()) {
			int startIndex = nodeMap.get(link.getStartNode());
			int endIndex = nodeMap.get(link.getEndNode());
			short[] startRow = futureMatrix[startIndex];
			short[] endRow = futureMatrix[endIndex];
			for (int i = 0; i < startRow.length; i++) {
				startRow[i] = or(startRow[i], endRow[i]);
			}
			for (int i = 0; i < startRow.length; i++) {
				if (futureMatrix[i][startIndex] == 1) {
					short[] ancRow = futureMatrix[i];
					for (int j = 0; j < startRow.length; j++) {
						ancRow[j] = or(ancRow[j], endRow[j]);
					}
				}
			}
		}
		double futureReachability = sumPathMatrix(futureMatrix)
				/ (double) MAX_REACHABILITY;
		// TODO: Insert formula here
		return futureReachability;
	}

	private short[][] copyPathMatrix() {
		short[][] pathMatrix = getPathMatrix();
		short[][] local = new short[pathMatrix.length][pathMatrix.length];
		for (int i = 0; i < pathMatrix.length; i++) {
			local[i] = pathMatrix[i].clone();
		}
		return local;
	}

	private short or(short op1, short op2) {
		if (op1 == 0 && op2 == 0) {
			return 0;
		}
		return 1;
	}

	protected double connectivity() {
		if (isStale) {
			double num = sumPathMatrix();
			LOGGER.debug("Numerator: " + num);
			double den = MAX_REACHABILITY;
			LOGGER.debug("Denominator" + den);
			reachability = num / den;
			isStale = false;
		}
		return reachability;
	}

	protected long sumPathMatrix(short[][] localMatrix) {
		long sum = 0;
		for (int i = 0; i < localMatrix[0].length; i++) {
			for (int j = 0; j < localMatrix[0].length; j++) {
				if (localMatrix[i][j] == 1) {
					sum += ((long) components.get(i).size() * (long) components
							.get(j).size());
				}
			}
		}
		return sum;
	}

	private long sumPathMatrix() {
		return sumPathMatrix(getPathMatrix());
	}

	public short[][] getPathMatrix() {
		if (isStale || matrix == null) {
			setUp();
			getPathMatrixFromAdjMatrix();
		}
		return matrix;
	}

	private void setUp() {
		Collections.sort(roadLinks);
		index = 0;
		stack = new LinkedList<>();
		matrix = getAdjacencyMatrix();
		setSuccessorEdges(roadLinks);
		for (Component c : components) {
			c.setTraverseIndex(-1);
			c.setInStack(false);
		}
	}

	private short[][] getAdjacencyMatrix() {
		int size = components.size();
		short[][] matrix = new short[size][size];
		for (int i = 0; i < size; i++) {
			matrix[i][i] = 1;
		}
		for (int i = 0; i < roadLinks.size(); i++) {
			RoadLink current = roadLinks.get(i);
			int row = nodeMap.get(current.getStartNode());
			int column = nodeMap.get(current.getEndNode());
			matrix[row][column] = 1;
		}
		return matrix;
	}

	// private int getComponentIndex(Node node) {
	// if (nodeMap.get(node) == null) {
	// nodeMap = createComponentMap(components);
	// }
	// try {
	// nodeMap.get(node);
	// } catch (NullPointerException e) {
	// LOGGER.error(node.toString());
	// LOGGER.error(nodeMap.get(node).toString());
	// }
	// if (nodeMap.get(node) == null) {
	// LOGGER.error(node.toString());
	// LOGGER.error(nodeMap.get(node).toString());
	// }
	// return nodeMap.get(node);
	// }

	private void setSuccessorEdges(List<RoadLink> roadLinks) {
		for (RoadLink roadLink : roadLinks) {
			int index = nodeMap.get(roadLink.getStartNode());
			components.get(index).addSuccessorEdge(roadLink);
		}
	}

	private void getPathMatrixFromAdjMatrix() {
		for (Component c : components) {
			if (c.isUnvisited()) {
				traverse(c);
			}
		}

		for (int i = 0; i < matrix.length; i++) {
			short[] row = matrix[i];
			for (int j = 0; j < row.length; j++) {
				if (row[j] == 1) {
					for (int k = 0; k < matrix.length; k++) {
						if (matrix[k][i] == 1) {
							matrix[k][j] = 1;
						}
					}
				}
			}
		}
	}

	private void traverse(Component vertex) {
		vertex.setTraverseIndex(index);
		index++;
		for (RoadLink edge : vertex.getSuccessorEdges()) {
			int toIndex = nodeMap.get(edge.getEndNode());
			for (Component node : stack) {
				matrix[node.getIndex()][toIndex] = 1;
			}
		}
		stack.push(vertex);
		vertex.setInStack(true);
		for (RoadLink edge : vertex.getSuccessorEdges()) {
			int toIndex = nodeMap.get(edge.getEndNode());
			Component to = components.get(toIndex);
			if (to.isUnvisited()) {
				traverse(to);
			} else if (to.isInStack()) {
				// TODO:Check if there are Array Operations in Java
				// basically short[] | short[]
				for (int i = 0; i < matrix[vertex.getIndex()].length; i++) {
					if (matrix[toIndex][i] == 1) {
						for (Component node : stack) {
							matrix[node.getIndex()][i] = 1;
						}
					}
				}
			}
		}
		stack.pop();
	}

	// For testing purposes
	protected void setComponents(List<Component> components) {
		this.components = components;
	}
}
