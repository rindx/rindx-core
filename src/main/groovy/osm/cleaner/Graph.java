package osm.cleaner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import osm.Node;
import osm.cleaner.RoadLink.Highway;
import osm.cleaner.distance.DistanceCalculator;
import osm.cleaner.distance.HalversineDistanceCalculator;

public class Graph {
	private static final Logger LOGGER = LoggerFactory.getLogger(Graph.class);
	private List<Node> nodes;
	private List<RoadLink> roadLinks;
	private List<RoadLink> brokenRoads;
	private TrajanAlgorithm algo;
	private List<Component> components;
	private List<SimpleNode> vertices;
	private boolean isStale;
	private int index;
	private Deque<SimpleNode> stack;
	private Matrix matrix;
	private DistanceCalculator calculator;

	public Graph(List<Node> nodes, List<RoadLink> roadLinks) {
		this(nodes, roadLinks, new TrajanAlgorithm());
	}

	private Graph(List<Node> nodes, List<RoadLink> roadLinks,
			TrajanAlgorithm algo) {
		this.nodes = nodes;
		this.roadLinks = roadLinks;
		this.algo = algo;
		this.calculator = new HalversineDistanceCalculator();
	}

	public List<RoadLink> findRoadLinksByName(String name) {
		List<RoadLink> foundLinks = new LinkedList<>();
		for (RoadLink roadLink : roadLinks) {
			if (name.equals(roadLink.getName())) {
				foundLinks.add(roadLink);
			}
		}
		return foundLinks;
	}

	public void addNode(Node node) {
		nodes.add(node);
		isStale = true;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void addRoadLink(RoadLink link) {
		roadLinks.add(link);
		isStale = true;
	}

	public void removeRoadLink(String startId, String endId) {
		int index = Collections.binarySearch(roadLinks, new RoadLink(new Node(
				startId), new Node(endId)));
		roadLinks.remove(index);
		isStale = true;
	}

	public void removeRoadLink(RoadLink roadLink) {
		roadLinks.remove(roadLink);
		isStale = true;
	}

	public List<RoadLink> getRoadLinks() {
		return roadLinks;
	}

	// Returns false if at least one node cannot be found
	public boolean addRoadLink(String fromId, String toId) {
		RoadLink link = createRoadLink(fromId, toId);
		boolean success = link.isValid();
		if (success) {
			addRoadLink(link);
		}
		return success;
	}

	public RoadLink createRoadLink(String fromId, String toId) {
		Node from = findNode(fromId);
		Node to = findNode(toId);
		return new RoadLink(from, to);
	}

	// TODO: Stopgap
	public RoadLink createRoadLinkWithType(String fromId, String toId,
			Highway type) {
		Node from = findNode(fromId);
		Node to = findNode(toId);
		return new RoadLink(from, to, type);
	}

	public Node findNode(String id) {
		int nodeIndex = Collections.binarySearch(nodes, new Node(id));
		if (nodeIndex < 0) {
			return Node.EMPTY;
		}
		return nodes.get(nodeIndex);
	}

	public List<Component> getComponents() {
		LOGGER.trace("Start Get Components");
		if (isStale || components == null) {
			components = algo.findComponents(nodes, roadLinks);
			Collections.sort(components);
			isStale = false;
		}
		LOGGER.trace("End Get Components");
		return components;
	}

	// Based on size of largest component
	public double componentConnectivity() {
		List<Component> temp = getComponents();
		double num = temp.get(temp.size() - 1).size();
		double den = nodes.size();
		return num / den;
	}

	public double pathMatrixConnectivity() {
		CondensedGraph condensed = condense();
		LOGGER.debug("Start Condense Connectivity");
		double local = condensed.connectivity();
		LOGGER.debug("End Condense Connectivity");
		return local;
	}

	public void setBrokenRoads(List<RoadLink> brokenRoads) {
		this.brokenRoads = brokenRoads;
	}

	// Assumes that numComponents > 1
	// Brute Force Solution
	// TODO: Add optimization function
	// How to return maxImpact? Create new Class?
	public RoadLink getBiggestImpact() {
		RoadLink maxRoad = new RoadLink(Node.EMPTY, Node.EMPTY);
		double maxConnectivity = 0;
		for (RoadLink road : brokenRoads) {
			Graph future = new Graph(new ArrayList<>(nodes), new ArrayList<>(
					roadLinks), algo);
			future.addRoadLink(road);
			double fConnectivity = future.componentConnectivity();
			// System.out.println(road.toString() + " : " + fConnectivity);
			if (fConnectivity > maxConnectivity) {
				maxConnectivity = fConnectivity;
				maxRoad = road;
			}
		}

		double impact = componentConnectivity() - maxConnectivity;
		// if(impact == 0) {
		// //Select the links that will connect the biggest and the second
		// biggest?
		// //But no guarantee of order. Damn.
		// }
		maxRoad.setPriority(impact);
		return maxRoad;
	}

	public List<RoadLink> repairPriority() {
		List<RoadLink> repairPriority = new ArrayList<>();
		double connectivity = componentConnectivity();
		updateBrokenRoads();

		while (connectivity < 1) {
			RoadLink maxRoad = getBiggestImpact();
			repairPriority.add(maxRoad);
			addRoadLink(maxRoad);
			brokenRoads.remove(maxRoad);
			updateBrokenRoads();
			connectivity = componentConnectivity();
		}
		return repairPriority;
	}

	private void updateBrokenRoads() {
		Collections.sort(brokenRoads);
		List<Component> localComponents = getComponents();
		List<Node> separatedNodes = new LinkedList<>();
		for (int i = 0; i < localComponents.size() - 1; i++) {
			separatedNodes.addAll(localComponents.get(i).getNodes());
		}
		List<RoadLink> filteredBrokenRoads = new LinkedList<>();
		Collections.sort(separatedNodes);
		for (RoadLink roadLink : brokenRoads) {
			Node startNode = roadLink.getStartNode();
			Node endNode = roadLink.getEndNode();
			if (inList(separatedNodes, startNode)
					|| inList(separatedNodes, endNode)) {
				filteredBrokenRoads.add(roadLink);
			}
		}
		brokenRoads = filteredBrokenRoads;
	}

	private boolean inList(List<Node> separatedNodes, Node node) {
		return Collections.binarySearch(separatedNodes, node) > -1;
	}

	public Matrix getPathMatrix() {
		if (isStale || matrix == null) {
			Collections.sort(nodes);
			Collections.sort(roadLinks);
			index = 0;
			stack = new LinkedList<>();
			matrix = getAdjacencyMatrix();
			this.vertices = toSimpleNode(nodes);
			setSucessorEdges(roadLinks);
			for (SimpleNode v : vertices) {
				if (v.isUnvisited()) {
					traverse(v);
				}
			}
		}
		return matrix;
	}

	private Matrix getAdjacencyMatrix() {
		int size = nodes.size();
		Matrix matrix = new MySquareMatrix(size);
		for (int i = 0; i < size; i++) {
			matrix.set(i, i, 1);
		}
		for (int i = 0; i < roadLinks.size(); i++) {
			RoadLink current = roadLinks.get(i);
			int row = Collections.binarySearch(nodes, current.getStartNode());
			int column = Collections.binarySearch(nodes, current.getEndNode());
			matrix.set(row, column, 1);
		}
		return matrix;
	}

	private void traverse(SimpleNode vertex) {
		vertex.index = index;
		index++;

		for (SimpleEdge edge : vertex.successorEdges) {
			SimpleNode to = edge.to;
			for (SimpleNode node : stack) {
				matrix.set(node.matrixIndex, to.matrixIndex, 1);
			}
		}

		stack.push(vertex);
		vertex.inStack = true;

		for (SimpleEdge edge : vertex.successorEdges) {
			SimpleNode to = edge.to;
			if (to.isUnvisited()) {
				traverse(to);
			} else if (to.inStack) {
				// TODO:Check if there are Array Operations in Java
				// basically short[] | short[]
				for (int i = 0; i < matrix.getRowsSize(); i++) {
					if (matrix.get(to.matrixIndex, i) == 1) {
						for (SimpleNode node : stack) {
							matrix.set(node.matrixIndex, i, 1);
						}
					}
				}
			}
		}
		stack.pop();
	}

	private void setSucessorEdges(List<RoadLink> roadLinks) {
		int counter = 0;
		for (SimpleNode vertex : vertices) {
			Node currentNode = vertex.node;
			vertex.successorEdges = new LinkedList<>();
			while (counter < roadLinks.size()
					&& currentNode
							.equals(roadLinks.get(counter).getStartNode())) {
				RoadLink currentRoad = roadLinks.get(counter);
				vertex.successorEdges.add(new SimpleEdge(currentRoad));
				counter++;
			}
		}

	}

	private List<SimpleNode> toSimpleNode(List<Node> nodes) {
		List<SimpleNode> vertices = new ArrayList<>();
		for (int i = 0; i < nodes.size(); i++) {
			vertices.add(new SimpleNode(nodes.get(i), i));
		}
		return vertices;
	}

	public String status() {
		StringBuilder builder = new StringBuilder();
		builder.append("Nodes: ").append(getNodes().size()).append("\n");
		builder.append("Edges: ").append(getRoadLinks().size()).append("\n");
		builder.append("Path Matrix Connectivity: ").append(
				pathMatrixConnectivity());
		return builder.toString();
	}

	public CondensedGraph condense() {
		LOGGER.trace("Start Condense");
		List<Component> comps = getComponents();
		List<RoadLink> condensedRoadLinks = condenseRoadLinks(comps);
		LOGGER.trace("End Condense");
		return new CondensedGraph(comps, condensedRoadLinks);
	}

	private List<RoadLink> condenseRoadLinks(List<Component> comps) {
		LOGGER.trace("Start Condense Road Links");
		List<RoadLink> condensedRoadLinks = new ArrayList<>();
		Map<Node, Integer> map = createComponentMap(comps);
		for (RoadLink roadLink : roadLinks) {
			Node start = roadLink.getStartNode();
			int startIndex = map.get(start);
			Node end = roadLink.getEndNode();
			int endIndex = map.get(end);
			if (startIndex != endIndex) {
				condensedRoadLinks.add(roadLink);
			}
		}
		Collections.sort(condensedRoadLinks);
		LOGGER.trace("End Condense Road Links");
		return condensedRoadLinks;
	}

	private Map<Node, Integer> createComponentMap(List<Component> comps) {
		Map<Node, Integer> map = new HashMap<>();
		for (int i = 0; i < comps.size(); i++) {
			Component c = comps.get(i);
			for (Node n : c.getNodes()) {
				map.put(n, i);
			}
		}
		return map;
	}

	public void removeRoadLinks(List<RoadLink> brokenRoads) {
		for (RoadLink roadLink : brokenRoads) {
			roadLinks.remove(Collections.binarySearch(roadLinks, roadLink));
		}
	}

	public void reduce() {
		GraphReducer reducer = new GraphReducer();
		reducer.reduce(this);
		isStale = true;
	}

	class SimpleEdge {
		SimpleNode from;
		SimpleNode to;

		public SimpleEdge(RoadLink link) {
			from = findVertex(link.getStartNode());
			to = findVertex(link.getEndNode());
		}

		private SimpleNode findVertex(Node vertex) {
			return vertices.get(Collections.binarySearch(vertices,
					new SimpleNode(vertex)));
		}
	}

	class SimpleNode implements Comparable<SimpleNode> {
		Node node;
		int index = -1;
		int matrixIndex = -1;
		List<SimpleEdge> successorEdges;
		boolean inStack = false;

		public SimpleNode(Node node) {
			this.node = node;
		}

		public SimpleNode(Node node, int matrixIndex) {
			this.node = node;
			this.matrixIndex = matrixIndex;
		}

		public boolean isUnvisited() {
			return index == -1;
		}

		@Override
		public int compareTo(SimpleNode o) {
			return node.compareTo(o.node);
		}
	}

	public List<Node> inRange(Node center, double radius) {
		List<Node> inRange = new ArrayList<>();
		for (Node n : nodes) {
			double distance = calculator.getDistance(center, n).doubleValue();
			if (distance <= radius) {
				inRange.add(n);
			}
		}
		return inRange;
	}

	public List<RoadLink> inRangeLinks(Node center, double radius) {
		List<Node> inRange = inRange(center, radius);
		List<RoadLink> inRangeLinks = new ArrayList<>();
		for (RoadLink link : roadLinks) {
			// TODO: Swap with binary search if too slow.
			if (inRange.contains(link.getStartNode())
					|| inRange.contains(link.getEndNode())) {
				inRangeLinks.add(link);
			}
		}
		return inRangeLinks;
	}

	// Threshold is the percent of connectivity to reduce.
	// Connectivity = 1 - Threshold
	public BrokenRoadData destroyLinksAround(Node center, double radius,
			double threshold) {
		List<RoadLink> inRangeLinks = inRangeLinks(center, radius);
		List<Road> inRangeRoads = Road.toRoads(inRangeLinks);
		LOGGER.info("Roads in range: " + inRangeRoads.size());
		List<Road> brokenRoads = new ArrayList<>();
		int count = 0;
		int total = inRangeRoads.size();
		LOGGER.trace("Start Connectivity");
		double connectivity = pathMatrixConnectivity();
		LOGGER.trace("End Connectivity");
		if (threshold == 1) {
			LOGGER.debug("FAST TRACK destroy");
			for (Road r : inRangeRoads) {
				brokenRoads.add(r);
				removeRoad(r);
			}
			inRangeRoads.clear();
			connectivity = pathMatrixConnectivity();
		} else {
			while (connectivity > (1 - threshold) && inRangeRoads.size() > 0) {
				int index = (int) (Math.random() * inRangeRoads.size());
				brokenRoads.add(inRangeRoads.get(index));
				removeRoad(inRangeRoads.get(index));
				inRangeRoads.remove(index);
				LOGGER.trace("Start Connectivity");
				connectivity = pathMatrixConnectivity();
				LOGGER.trace("End Connectivity");
				count++;
				LOGGER.debug("Destroyed {}/{} - {}", count, total, connectivity);
			}
		}
		// if (inRangeRoads.size() == 0) {
		// LOGGER.error("Not enough links to reach threshold");
		// throw new ExperimentException("Not enough links to reach threshold");
		// }
		BrokenRoadData data = new BrokenRoadData();
		data.setBrokenRoads(brokenRoads);
		data.setTargetThreshold(threshold);
		data.setActualThreshold(1 - connectivity);
		return data;
	}

	// TODO: Add tests for attempting to remove a Road whose links are not in
	// the Graph? Or move it down to removeRoadLink?
	public void removeRoad(Road road) {
		for (RoadLink link : road.getRoadLinks()) {
			removeRoadLink(link);
		}
	}

	public void addRoad(Road road) {
		for (RoadLink link : road.getRoadLinks()) {
			addRoadLink(link);
		}

	}

	public Map<Node, Integer> getANDegree() {
		Map<Node, Set<Node>> degreesWithSet = new HashMap<>();
		for (Node n : nodes) {
			degreesWithSet.put(n, new HashSet<>());
		}
		for (RoadLink r : roadLinks) {
			Node start = r.getStartNode();
			Node end = r.getEndNode();
			degreesWithSet.get(start).add(end);
			degreesWithSet.get(end).add(start);
		}
		Map<Node, Integer> degrees = new HashMap<>();
		for (Node n : nodes) {
			int degree = degreesWithSet.get(n).size();
			degrees.put(n, degree);
		}
		return degrees;
	}

	public String getDegrees() {
		for (RoadLink r : roadLinks) {
			Node startNode = findNode(r.getStartNode().getId());
			startNode.incOutdegree();
			Node endNode = findNode(r.getEndNode().getId());
			endNode.incIndegree();
		}
		StringBuilder builder = new StringBuilder();
		for (Node n : nodes) {
			builder.append(n.getId()).append(",").append(n.getIndegree())
					.append(",").append(n.getOutdegree()).append(",")
					.append(n.getIndegree() + n.getOutdegree()).append("\n");
		}

		return builder.toString();
	}
}