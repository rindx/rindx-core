package osm.cleaner;

import java.util.Arrays;

public class MySquareMatrix implements Matrix {
	private short[][] values;

	public MySquareMatrix(int side) {
		values = new short[side][side];
	}

	public MySquareMatrix(short[][] values) {
		this.values = new short[values.length][values.length];
		for(int i = 0; i < values.length; i++) {
			this.values[i] = values[i].clone();
		}
	}

	@Override
	public int get(int row, int column) {
		return values[row][column];
	}

	@Override
	public void set(int row, int column, int value) {
		values[row][column] = (short) value;
	}

	@Override
	public int getRowsSize() {
		return values.length;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(values);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MySquareMatrix other = (MySquareMatrix) obj;
		if (!Arrays.deepEquals(values, other.values))
			return false;
		return true;
	}
	
	
}
