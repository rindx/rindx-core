package osm.cleaner;

import java.util.LinkedList;
import java.util.List;

import osm.Node;

public class Box {
	private List<Node> nodes;
	
	public Box() {
		nodes = new LinkedList<>();
	}

	public void add(Node n) {
		nodes.add(n);
	}

	public List<Node> getNodes() {
		return nodes;
	}

	@Override
	public String toString() {
		return nodes.toString();
	}
 
	
}
