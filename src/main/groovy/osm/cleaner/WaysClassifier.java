package osm.cleaner;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import osm.Tag;
import osm.Way;

public class WaysClassifier {
	private static final String USED_TXT = "used.txt";
	private static final String USED_IGNORE_TXT = "usedIgnore.txt";
	private static final String COMPRESS_TXT = "compress.txt";
	private List<Tag> usedTags;
	private List<Tag> usedIgnoreTags;
	private List<Tag> compressTags;

	public WaysClassifier() {
		this(USED_TXT, USED_IGNORE_TXT, COMPRESS_TXT);
	}

	public WaysClassifier(String usedTagsFile, String usedIgnoreTagsFile,
			String compressTagsFile) {
		this.usedTags = readTags(usedTagsFile);
		this.usedIgnoreTags = readTags(usedIgnoreTagsFile);
		this.compressTags = readTags(compressTagsFile);
	}

	public WaysClassifier(List<Tag> usedTags, List<Tag> usedIgnoreTags,
			List<Tag> compressTags) {
		this.usedTags = usedTags;
		this.usedIgnoreTags = usedIgnoreTags;
		this.compressTags = compressTags;
	}

	protected List<Tag> readTags(String filename) {
		List<Tag> tags = new ArrayList<>();
		try (Scanner scanner = new Scanner(new FileReader(filename))) {
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				tags.add(createTag(line));
			}
		} catch (IOException e) {
			throw new DataCleaningException(e);
		}
		return tags;
	}

	public boolean isCompress(Way way) {
		for (Tag t : compressTags) {
			if (way.getTagList().contains(t)) {
				return true;
			}
		}
		return false;
	}

	protected Tag createTag(String line) {
		String[] tagLine = line.split("=");
		Tag tag = new Tag(tagLine[0], tagLine[1]);
		return tag;
	}

	protected boolean isUsed(Way w) {
		for (Tag t : usedTags) {
			ArrayList<Tag> tagList = w.getTagList();

			if ("*".equals(t.getV())) {
				for (Tag g : tagList) {
					if (!isIgnored(g) && g.getK().equals(t.getK())) {
						return true;
					}
				}
			} else if (tagList.contains(t)) {
				return true;
			}
		}
		return false;
	}

	private boolean isIgnored(Tag g) {
		return usedIgnoreTags.contains(g);
	}

	protected void setUsedTags(List<Tag> usedTags) {
		this.usedTags = usedTags;
	}

	protected void setUsedIgnoreTags(List<Tag> usedIgnoreTags) {
		this.usedIgnoreTags = usedIgnoreTags;
	}

	protected void setCompressTags(List<Tag> compressTags) {
		this.compressTags = compressTags;
	}

	public List<Tag> getUsedTags() {
		return usedTags;
	}

	public List<Tag> getUsedIgnoreTags() {
		return usedIgnoreTags;
	}

	public List<Tag> getCompressTags() {
		return compressTags;
	}

}
