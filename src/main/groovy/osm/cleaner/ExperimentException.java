package osm.cleaner;

public class ExperimentException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ExperimentException() {
		// TODO Auto-generated constructor stub
	}

	public ExperimentException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExperimentException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ExperimentException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ExperimentException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
