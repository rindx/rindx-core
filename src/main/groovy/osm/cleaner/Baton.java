package osm.cleaner;

import osm.Node;

public class Baton {
	private Node node;
	private RoadLink link;
	
	//For Equals
	protected Baton(Node node) {
		this.node = node;
	}

	public Baton(Node node, RoadLink link) {
		this.node = node;
		this.link = link;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public RoadLink getLink() {
		return link;
	}

	public void setLink(RoadLink link) {
		this.link = link;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Baton other = (Baton) obj;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		return true;
	}

}
