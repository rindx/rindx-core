package osm.cleaner;

import java.math.BigDecimal;

import osm.Node;
import osm.cleaner.distance.HalversineDistanceCalculator;

//TODO: Deal with issues of changing nodes. May affect labels.
public class RoadLink implements Comparable<RoadLink> {
	private static final String DEFAULT_DELIMITER = ";";

	public enum Highway {
		MOTORWAY, MOTORWAY_LINK, TRUNK, TRUNK_LINK, PRIMARY, PRIMARY_LINK, SECONDARY, SECONDARY_LINK, TERTIARY, TERTIARY_LINK, UNCLASSIFIED, SERVICE, BUILDING_LINK
	}

	private Node startNode;
	private Node endNode;
	private String name;
	private BigDecimal distance;
	private int lanes;
	private double priority;
	private String delimiter;
	private Highway highwayType;

	public RoadLink(Node start, Node end) {
		this(start, end, start.getId() + end.getId(), new HalversineDistanceCalculator().getDistance(start, end));
	}

	public RoadLink(Node start, Node end, String name, BigDecimal distance) {
		this(start, end, name, distance, DEFAULT_DELIMITER);
	}

	public RoadLink(Node start, Node end, String name, BigDecimal distance,
			int lanes) {
		this(start, end, name, distance, lanes, DEFAULT_DELIMITER);
	}

	public RoadLink(Node start, Node end, String name, BigDecimal distance,
			String delimiter) {
		this(start, end, name, distance, 2, delimiter);
	}

	public RoadLink(Node start, Node end, String name, BigDecimal distance,
			int lanes, String delimiter) {
		this.name = name;
		this.distance = distance;
		this.delimiter = delimiter;
		this.startNode = start;
		this.endNode = end;
		this.lanes = lanes;
	}
	
	public RoadLink(Node start, Node end, Highway type) {
		this(start, end, start.getId() + end.getId(), BigDecimal.ONE, 2, type);
	}

	public RoadLink(Node start, Node end, String name, BigDecimal distance,
			int lanes, Highway type) {
		this.name = name;
		this.distance = distance;
		this.delimiter = DEFAULT_DELIMITER;
		this.startNode = start;
		this.endNode = end;
		this.lanes = lanes;
		this.highwayType = type;
	}
	
	public RoadLink reverse() {
		return new RoadLink(endNode, startNode, name, distance, lanes, highwayType);
	}

	public boolean isVertical() {
		try {
			getSlope();
		} catch (ArithmeticException e) {
			return true;
		}
		return false;
	}

	public BigDecimal getSlope() {
		BigDecimal y2 = new BigDecimal(startNode.getLat());
		BigDecimal x2 = new BigDecimal(startNode.getLon());
		BigDecimal y1 = new BigDecimal(endNode.getLat());
		BigDecimal x1 = new BigDecimal(endNode.getLon());
		BigDecimal num = y2.subtract(y1);
		BigDecimal den = x2.subtract(x1);
		BigDecimal slope = num.divide(den, 7, BigDecimal.ROUND_HALF_DOWN);
		return slope.setScale(7);
	}

	public BigDecimal getDistance() {
		return distance;
	}

	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public Node getStartNode() {
		return startNode;
	}

	public void setStartNode(Node startNode) {
		this.startNode = startNode;
	}

	public Node getEndNode() {
		return endNode;
	}

	public void setEndNode(Node endNode) {
		this.endNode = endNode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(startNode.getId()).append(delimiter);
		builder.append(endNode.getId()).append(delimiter);
		builder.append(name).append(delimiter);
		builder.append(distance).append(delimiter);
		builder.append(lanes);
		if (highwayType != null) {
			builder.append(delimiter).append(highwayType);
		}
		return builder.toString();
	}

	public boolean isHorizontal() {
		return getSlope().compareTo(BigDecimal.ZERO) == 0;
	}

	// TODO: Refactor
	public double distanceFrom(Node node) {
		double value = 0;
		double x1 = Double.parseDouble(startNode.getLon());
		double y1 = Double.parseDouble(startNode.getLat());
		double x2 = Double.parseDouble(endNode.getLon());
		double y2 = Double.parseDouble(endNode.getLat());
		double x3 = Double.parseDouble(node.getLon());
		double y3 = Double.parseDouble(node.getLat());

		double px = x2 - x1;
		double py = y2 - y1;

		double den = (px * px) + (py * py);

		double v = (((x3 - x1) * px) + ((y3 - y1) * py)) / den;

		if (v > 1) {
			v = 1;
		} else if (v < 0) {
			v = 0;
		}

		double x = x1 + (v * px);
		double y = y1 + (v * py);

		double dx = (x - x3) * (x - x3);
		double dy = (y - y3) * (y - y3);

		value = Math.sqrt(dx + dy);

		return value;
	}

	@Override
	public int compareTo(RoadLink o) {
		int startCmp = startNode.compareTo(o.getStartNode());
		if (startCmp != 0) {
			return startCmp;
		}
		return endNode.compareTo(o.getEndNode());
	}

	public boolean isValid() {
		return !startNode.isEmpty() && !endNode.isEmpty();
	}

	public double getPriority() {
		return priority;
	}

	public void setPriority(double priority) {
		this.priority = priority;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endNode == null) ? 0 : endNode.hashCode());
		result = prime * result
				+ ((startNode == null) ? 0 : startNode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoadLink other = (RoadLink) obj;
		if (endNode == null) {
			if (other.endNode != null)
				return false;
		} else if (!endNode.equals(other.endNode))
			return false;
		if (startNode == null) {
			if (other.startNode != null)
				return false;
		} else if (!startNode.equals(other.startNode))
			return false;
		return true;
	}

	public int getLanes() {
		return lanes;
	}

	public Highway getHighwayType() {
		return highwayType;
	}
}
