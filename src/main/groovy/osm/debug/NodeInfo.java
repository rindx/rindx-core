package osm.debug;

import java.util.List;

public class NodeInfo {
	private String id;
	private List<String> info;

	public NodeInfo(String id, List<String> info) {
		this.id = id;
		this.info = info;
	}

	public String getId() {
		return id;
	}

	public List<String> getInfo() {
		return info;
	}

	@Override
	public String toString() {
		return "id=" + id + ", info=" + info;
	}

	public String toStringTable() {
		StringBuilder builder = new StringBuilder();
		for(String s: info) {
			builder.append(id).append("\t").append(s).append("\n");
		}
		return builder.toString();
	}
	
}
