package osm;

import static osm.TestUtils.*;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import osm.cleaner.RoadLink;
import osm.cleaner.RoadLink.Highway;

public class FileUtilsTest {

	@Test(expected = FileNotFoundException.class)
	public void readNodesFileNotFoundTest() throws FileNotFoundException {
		FileUtils.readNodes(resource("nodes-FNF.txt"));
	}

	@Test
	public void readNodesTest() throws FileNotFoundException {
		List<Node> nodes = FileUtils.readNodes(resource("nodes.txt"));
		List<Node> expected = new ArrayList<>();
		expected.add(new Node("A", "lat", "lon"));
		expected.add(new Node("B", "lat", "lon"));
		expected.add(new Node("C", "lat", "lon"));
		expected.add(new Node("D", "lat", "lon"));
		expected.add(new Node("E", "lat", "lon"));
		expected.add(new Node("F", "lat", "lon"));
		expected.add(new Node("G", "lat", "lon"));
		expected.add(new Node("H", "lat", "lon"));
		expected.add(new Node("I", "lat", "lon"));
		expected.add(new Node("J", "lat", "lon"));
		expected.add(new Node("K", "lat", "lon"));
		expected.add(new Node("L", "lat", "lon"));
		expected.add(new Node("M", "lat", "lon"));

		assertNodes(expected, nodes);
	}

	@Test
	public void readRoadlinksTest() throws FileNotFoundException {
		List<RoadLink> roadLinks = FileUtils
				.readRoadlinks(resource("roadlinks-02.txt"));
		List<RoadLink> expected = new ArrayList<>();
		expected.add(new RoadLink(new Node("A"), new Node("B"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("B"), new Node("A"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("A"), new Node("C"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("C"), new Node("A"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("A"), new Node("D"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("D"), new Node("A"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("B"), new Node("C"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("C"), new Node("B"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("B"), new Node("D"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("D"), new Node("B"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("C"), new Node("D"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		expected.add(new RoadLink(new Node("D"), new Node("C"), "",
				new BigDecimal("1.0"), 2, Highway.PRIMARY));
		assertRoadlinks(expected, roadLinks);
	}

}
