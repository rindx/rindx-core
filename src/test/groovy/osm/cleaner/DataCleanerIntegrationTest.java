package osm.cleaner;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import osm.Bounds;
import osm.Tag;
import osm.Way;

public class DataCleanerIntegrationTest {
	private static final String CLEANER_TEST_RESOURCES = "src/test/resources/osm/cleaner/";
	private DataCleaner cleaner;

	@Before
	public void setUp() {
		cleaner = new DataCleaner(new Bounds(), createWaysClassifier());
	}

	private WaysClassifier createWaysClassifier() {
		String usedTagsFile = CLEANER_TEST_RESOURCES + "used.txt";
		String usedIgnoreTagsFile = CLEANER_TEST_RESOURCES + "usedIgnore.txt";
		String compressTagsFile = CLEANER_TEST_RESOURCES + "compress.txt";
		WaysClassifier classifier = new WaysClassifier(usedTagsFile, usedIgnoreTagsFile, compressTagsFile);
		return classifier;
	}

	@Test
	public void isCompressTest() {
		Way way = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("building", "yes"));
		way.setTagList(tagList);
		assertTrue(cleaner.isCompress(way));
	}

	@Test
	public void isCompressFalseTest() {
		Way way = new Way();
		assertFalse(cleaner.isCompress(way));

		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("building", "no"));
		way.setTagList(tagList);
		assertFalse(cleaner.isCompress(way));
	}

	@Test
	public void isUsedTest() {
		Way way = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("highway", "primary"));
		way.setTagList(tagList);
		assertTrue(cleaner.isUsed(way));
	}

	@Test
	public void isUsedIgnoreTest() {
		Way way = new Way();
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(new Tag("highway", "footway"));
		way.setTagList(tagList);
		assertFalse(cleaner.isUsed(way));
	}

}
