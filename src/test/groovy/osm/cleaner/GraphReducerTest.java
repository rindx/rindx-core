package osm.cleaner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class GraphReducerTest {
	private GraphReducer reducer;
	private List<Node> nodes;
	private List<RoadLink> roadLinks;
	private Graph graph;

	@Before
	public void setUp() {
		reducer = new GraphReducer();
		nodes = new ArrayList<>();
		roadLinks = new ArrayList<>();
		graph = new Graph(nodes, roadLinks);
	}

	@Test
	public void twowayIntersectionTest() {
		Node nodeA = addNode("A");
		Node nodeB = addNode("B");
		Node nodeC = addNode("C");
		Node nodeD = addNode("D");
		addNode("E");
		Node nodeF = addNode("F");
		Node nodeG = addNode("G");
		Node nodeH = addNode("H");
		Node nodeI = addNode("I");
		RoadLink linkAD = addRoadLink("AD");
		RoadLink linkDA = addRoadLink("DA");
		RoadLink linkBF = addRoadLink("BF");
		RoadLink linkFB = addRoadLink("FB");
		RoadLink linkCD = addRoadLink("CD");
		RoadLink linkDC = addRoadLink("DC");
		addRoadLink("DE");
		addRoadLink("ED");
		addRoadLink("EF");
		addRoadLink("FE");
		RoadLink linkFG = addRoadLink("FG");
		RoadLink linkGF = addRoadLink("GF");
		RoadLink linkDH = addRoadLink("DH");
		RoadLink linkHD = addRoadLink("HD");
		RoadLink linkFI = addRoadLink("FI");
		RoadLink linkIF = addRoadLink("IF");

		reducer.reduce(graph);

		assertEquals(9, nodes.size());
		assertEquals("A", nodeA.getId());
		testOut(nodeA, nodeD, linkAD);
		testIn(nodeA, nodeD, linkDA);
		assertEquals("B", nodeB.getId());
		testOut(nodeB, nodeF, linkBF);
		testIn(nodeB, nodeF, linkFB);
		assertEquals("C", nodeC.getId());
		testOut(nodeC, nodeD, linkCD);
		testIn(nodeC, nodeD, linkDC);
		assertEquals("D", nodeD.getId());
		RoadLink expectedLinkDF = new RoadLink(nodeD, nodeF, "DE",
				new BigDecimal("2"));
		List<RoadLink> actualRoadLinks = graph.findRoadLinksByName("DE");
		assertEquals(1, actualRoadLinks.size());
		RoadLink actualRoadLink = actualRoadLinks.get(0);
		assertRoadlink(expectedLinkDF, actualRoadLink);
		testOut(nodeD, toList(nodeA, nodeC, nodeF, nodeH),
				toList(linkDA, linkDC, expectedLinkDF, linkDH));
		RoadLink expectedLinkFD = new RoadLink(nodeF, nodeD, "FE",
				new BigDecimal("2"));
		List<RoadLink> actualRoadLinks2 = graph.findRoadLinksByName("FE");
		assertEquals(1, actualRoadLinks2.size());
		RoadLink actualRoadLink2 = actualRoadLinks2.get(0);
		assertRoadlink(expectedLinkFD, actualRoadLink2);
		testIn(nodeD, toList(nodeA, nodeC, nodeF, nodeH),
				toList(linkAD, linkCD, expectedLinkFD, linkHD));
		assertEquals("F", nodeF.getId());
		testOut(nodeF, toList(nodeB, nodeD, nodeG, nodeI),
				toList(linkFB, expectedLinkFD, linkFG, linkFI));
		testIn(nodeF, toList(nodeB, nodeD, nodeG, nodeI),
				toList(linkBF, expectedLinkDF, linkGF, linkIF));
		assertEquals("G", nodeG.getId());
		testOut(nodeG, nodeF, linkGF);
		testIn(nodeG, nodeF, linkFG);
		assertEquals("H", nodeH.getId());
		testOut(nodeH, nodeD, linkHD);
		testIn(nodeH, nodeD, linkDH);
		assertEquals("I", nodeI.getId());
		testOut(nodeI, nodeF, linkIF);
		testIn(nodeI, nodeF, linkFI);
	}

	private void assertRoadlink(RoadLink expected, RoadLink actual) {
		assertEquals(expected.getStartNode(), actual.getStartNode());
		assertEquals(expected.getEndNode(), actual.getEndNode());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getDistance(), actual.getDistance());
	}

	@Test
	public void onewayHasDirectLinkTest() {
		Node nodeA = addNode("A");
		Node nodeB = addNode("B");
		Node nodeC = addNode("C");
		Node nodeD = addNode("D");
		Node nodeE = addNode("E");
		Node nodeF = addNode("F");
		RoadLink linkAB = addRoadLink("AB");
		RoadLink linkBC = addRoadLink("BC");
		RoadLink linkBE = addRoadLink("BE");
		RoadLink linkCD = addRoadLink("CD");
		RoadLink linkDE = addRoadLink("DE");
		RoadLink linkEF = addRoadLink("EF");

		reducer.reduce(graph);

		assertEquals(6, nodes.size());
		assertEquals("A", nodeA.getId());
		testOut(nodeA, nodeB, linkAB);
		assertInEmpty(nodeA);
		assertEquals("B", nodeB.getId());
		testOut(nodeB, toList(nodeC, nodeE), toList(linkBC, linkBE));
		testIn(nodeB, nodeA, linkAB);
		assertEquals("C", nodeC.getId());
		testOut(nodeC, nodeD, linkCD);
		testIn(nodeC, nodeB, linkBC);
		assertEquals("D", nodeD.getId());
		testOut(nodeD, nodeE, linkDE);
		testIn(nodeD, nodeC, linkCD);
		assertEquals("E", nodeE.getId());
		testOut(nodeE, nodeF, linkEF);
		testIn(nodeE, toList(nodeB, nodeD), toList(linkBE, linkDE));
		assertEquals("F", nodeF.getId());
		assertOutEmpty(nodeF);
		testIn(nodeF, nodeE, linkEF);

		assertEquals(6, roadLinks.size());
		assertEquals(linkAB, roadLinks.get(0));
		assertEquals(linkBC, roadLinks.get(1));
		assertEquals(linkBE, roadLinks.get(2));
		assertEquals(linkCD, roadLinks.get(3));
		assertEquals(linkDE, roadLinks.get(4));
		assertEquals(linkEF, roadLinks.get(5));
	}

	@SuppressWarnings("unchecked")
	private <T> List<T> toList(T... objects) {
		List<T> objectList = new ArrayList<>();
		for (T o : objects) {
			objectList.add(o);
		}
		return objectList;
	}

	@Test
	public void cycleTest() {
		Node nodeA = addNode("A");
		Node nodeB = addNode("B");
		Node nodeC = addNode("C");
		RoadLink linkAB = addRoadLink("AB");
		RoadLink linkBC = addRoadLink("BC");
		RoadLink linkCA = addRoadLink("CA");

		reducer.reduce(graph);

		assertEquals(3, nodes.size());
		assertEquals("A", nodeA.getId());
		testOut(nodeA, nodeB, linkAB);
		testIn(nodeA, nodeC, linkCA);
		assertEquals("B", nodeB.getId());
		testOut(nodeB, nodeC, linkBC);
		testIn(nodeB, nodeA, linkAB);
		assertEquals("C", nodeC.getId());
		testOut(nodeC, nodeA, linkCA);
		testIn(nodeC, nodeB, linkBC);

		assertEquals(3, roadLinks.size());
		assertEquals(linkAB, roadLinks.get(0));
		assertEquals(linkBC, roadLinks.get(1));
		assertEquals(linkCA, roadLinks.get(2));
	}

	@Test
	public void onewayLineTest() {
		Node nodeA = addNode("A");
		addNode("B");
		Node nodeC = addNode("C");
		addRoadLink("AB");
		addRoadLink("BC");

		reducer.reduce(graph);

		assertEquals(3, nodes.size());
		RoadLink expectedLink = new RoadLink(nodeA, nodeC, "AB",
				new BigDecimal("2"));
		assertEquals("A", nodeA.getId());
		testOut(nodeA, nodeC, expectedLink);
		assertInEmpty(nodeA);
		assertEquals("C", nodeC.getId());
		testIn(nodeC, nodeA, expectedLink);
		assertOutEmpty(nodeC);
		assertEquals(1, roadLinks.size());
		assertEquals(expectedLink, roadLinks.get(0));
	}

	@Test
	public void onewayLongLineTest() {
		Node nodeA = addNode("A");
		addNode("B");
		addNode("C");
		addNode("D");
		addNode("E");
		addNode("F");
		addNode("G");
		Node nodeH = addNode("H");
		addRoadLink("AB");
		addRoadLink("BC");
		addRoadLink("CD");
		addRoadLink("DE");
		addRoadLink("EF");
		addRoadLink("FG");
		addRoadLink("GH");

		reducer.reduce(graph);

		assertEquals(8, nodes.size());
		RoadLink expectedLink = new RoadLink(nodeA, nodeH, "AB",
				new BigDecimal("3"));
		assertEquals("A", nodeA.getId());
		testOut(nodeA, nodeH, expectedLink);
		assertInEmpty(nodeA);
		assertEquals("H", nodeH.getId());
		testIn(nodeH, nodeA, expectedLink);
		assertOutEmpty(nodeH);

		assertEquals(1, roadLinks.size());
		assertEquals(expectedLink, roadLinks.get(0));
	}

	@Test
	public void twowayLineTest() {
		Node nodeA = addNode("A");
		addNode("B");
		Node nodeC = addNode("C");
		addRoadLink("AB");
		addRoadLink("BA");
		addRoadLink("BC");
		addRoadLink("CB");

		reducer.reduce(graph);

		assertEquals(3, nodes.size());
		RoadLink forwardLink = new RoadLink(nodeA, nodeC, "AB", new BigDecimal(
				"2"));
		RoadLink backLink = new RoadLink(nodeC, nodeA, "CB",
				new BigDecimal("2"));
		assertEquals("A", nodeA.getId());
		testOut(nodeA, nodeC, forwardLink);
		testIn(nodeA, nodeC, backLink);
		assertEquals("C", nodeC.getId());
		testIn(nodeC, nodeA, forwardLink);
		testOut(nodeC, nodeA, backLink);

		assertEquals(2, roadLinks.size());
		assertEquals(forwardLink, roadLinks.get(0));
		assertEquals(backLink, roadLinks.get(1));
	}

	@Test
	public void twowayLongLineTest() {
		Node nodeA = addNode("A");
		addNode("B");
		addNode("C");
		addNode("D");
		addNode("E");
		addNode("F");
		addNode("G");
		Node nodeH = addNode("H");
		addRoadLink("AB");
		addRoadLink("BA");
		addRoadLink("BC");
		addRoadLink("CB");
		addRoadLink("CD");
		addRoadLink("DC");
		addRoadLink("DE");
		addRoadLink("ED");
		addRoadLink("EF");
		addRoadLink("FE");
		addRoadLink("FG");
		addRoadLink("GF");
		addRoadLink("GH");
		addRoadLink("HG");

		reducer.reduce(graph);

		assertEquals(8, nodes.size());
		RoadLink forwardLink = new RoadLink(nodeA, nodeH, "AB", new BigDecimal(
				"3"));
		RoadLink backLink = new RoadLink(nodeH, nodeA, "HG",
				new BigDecimal("3"));
		assertEquals("A", nodeA.getId());
		testOut(nodeA, nodeH, forwardLink);
		testIn(nodeA, nodeH, backLink);
		assertEquals("H", nodeH.getId());
		testIn(nodeH, nodeA, forwardLink);
		testOut(nodeH, nodeA, backLink);

		assertEquals(2, roadLinks.size());
		assertEquals(forwardLink, roadLinks.get(0));
		assertEquals(backLink, roadLinks.get(1));
	}

	private void assertOutEmpty(Node node) {
		assertTrue(node.getOutNodes().isEmpty());
		assertTrue(node.getOutBatons().isEmpty());
	}

	private void assertInEmpty(Node node) {
		assertTrue(node.getInNodes().isEmpty());
		assertTrue(node.getInBatons().isEmpty());
	}

	private RoadLink addRoadLink(String name) {
		RoadLink roadLink = createRoadLink(name);
		roadLinks.add(roadLink);
		return roadLink;
	}

	private RoadLink createRoadLink(String name) {
		String startId = "" + name.charAt(0);
		String endId = "" + name.charAt(1);
		RoadLink roadLink = new RoadLink(findNode(startId), findNode(endId),
				name, BigDecimal.ONE);
		return roadLink;
	}

	private Node findNode(String nodeId) {
		return nodes.get(nodes.indexOf(new Node(nodeId)));
	}

	private Node addNode(String nodeId) {
		Node node = new Node(nodeId);
		nodes.add(node);
		return node;
	}

	private void testIn(Node node, Node inNode, RoadLink link) {
		List<Node> inNodes = node.getInNodes();
		assertEquals(1, inNodes.size());
		assertEquals(inNode, inNodes.get(0));
		List<Baton> inBatons = node.getInBatons();
		assertEquals(1, inBatons.size());
		assertEquals(new Baton(inNode, link), inBatons.get(0));
		assertNodesInLink(inNode, node, link);
	}

	private void testIn(Node node, List<Node> expectedInNodes,
			List<RoadLink> link) {
		List<Node> inNodes = node.getInNodes();
		assertEquals(expectedInNodes.size(), inNodes.size());
		for (Node e : expectedInNodes) {
			assertTrue(inNodes.contains(e));
		}
		List<Baton> inBatons = node.getInBatons();
		assertEquals(expectedInNodes.size(), inBatons.size());
		for (int i = 0; i < expectedInNodes.size(); i++) {
			assertTrue(inBatons.contains(new Baton(expectedInNodes.get(i), link
					.get(i))));
		}
		for (int i = 0; i < link.size(); i++) {
			assertNodesInLink(expectedInNodes.get(i), node, link.get(i));
		}
	}

	private void testOut(Node node, Node outNode, RoadLink link) {
		List<Node> outNodes = node.getOutNodes();
		assertEquals(1, outNodes.size());
		assertEquals(outNode, outNodes.get(0));
		List<Baton> outBatons = node.getOutBatons();
		assertEquals(1, outBatons.size());
		// What exactly is this testing?
		assertEquals(new Baton(outNode, link), outBatons.get(0));
		// This sounds more like it.
		assertNodesInLink(node, outNode, link);
	}

	private void testOut(Node node, List<Node> expectedOutNodes,
			List<RoadLink> link) {
		List<Node> outNodes = node.getOutNodes();
		assertEquals(expectedOutNodes.size(), outNodes.size());
		for (Node e : expectedOutNodes) {
			assertTrue(outNodes.contains(e));
		}
		List<Baton> outBatons = node.getOutBatons();
		assertEquals(expectedOutNodes.size(), outBatons.size());
		for (int i = 0; i < expectedOutNodes.size(); i++) {
			assertTrue(outBatons.contains(new Baton(expectedOutNodes.get(i),
					link.get(i))));
		}
		for (int i = 0; i < link.size(); i++) {
			assertNodesInLink(node, expectedOutNodes.get(i), link.get(i));
		}
	}

	private void assertNodesInLink(Node node, Node outNode, RoadLink link) {
		assertEquals(node, link.getStartNode());
		assertEquals(outNode, link.getEndNode());
	}

}
