package osm.cleaner.distance

import osm.Node
import spock.lang.Specification


class HalversineDistanceCalculatorSpec extends Specification {
    static final BigDecimal RADIUS = 6371
    DistanceCalculator distanceCalculator = new HalversineDistanceCalculator(RADIUS)

    def 'Should get distance in kilometers'() {
        setup:
        Node from = new Node("A", "14.6503180", "121.0520222")
        Node to = new Node("B", "14.6501908", "121.0534853")

        expect:
        0.1580 == distanceCalculator.getDistance(from, to)
    }
}
