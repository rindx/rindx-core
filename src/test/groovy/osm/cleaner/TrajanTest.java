package osm.cleaner;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class TrajanTest {
	TrajanAlgorithm trajan;
	private List<Node> nodes;
	private List<RoadLink> roadLinks;

	@Before
	public void setUp() {
		trajan = new TrajanAlgorithm();
		nodes = new ArrayList<>();
		roadLinks = new ArrayList<>();
	}

	@Test
	public void fourComponentTest() {
		Node nodeA = addNode("A");
		Node nodeB = addNode("B");
		Node nodeC = addNode("C");
		Node nodeD = addNode("D");
		Node nodeE = addNode("E");
		Node nodeF = addNode("F");
		Node nodeG = addNode("G");
		Node nodeH = addNode("H");
		addRoadLink("BA");
		addRoadLink("AE");
		addRoadLink("EB");
		addRoadLink("FE");
		addRoadLink("FB");
		addRoadLink("CB");
		addRoadLink("FG");
		addRoadLink("GF");
		addRoadLink("GC");
		addRoadLink("CD");
		addRoadLink("DC");
		addRoadLink("HD");
		addRoadLink("HG");
		assertEquals(4, trajan.getNumComponents(nodes,roadLinks));
		List<Component> components = trajan.findComponents(nodes,roadLinks);
		assertEquals(4, components.size());
		
		List<Node> expected = new ArrayList<>();
		expected.add(nodeA);
		expected.add(nodeB);
		expected.add(nodeE);
		assertTrue(components.get(0).containsAll(expected));
		
		expected.clear();
		expected.add(nodeC);
		expected.add(nodeD);
		assertTrue(components.get(1).containsAll(expected));
		
		expected.clear();
		expected.add(nodeF);
		expected.add(nodeG);
		assertTrue(components.get(2).containsAll(expected));
		
		expected.clear();
		expected.add(nodeH);
		assertTrue(components.get(3).containsAll(expected));
	}
	
	@Test
	public void startFromPureSourceTest() {
		Node nodeA = addNode("A");
		Node nodeB = addNode("B");
		Node nodeC = addNode("C");
		Node nodeD = addNode("D");
		Node nodeE = addNode("E");
		Node nodeF = addNode("F");
		Node nodeG = addNode("G");
		Node nodeH = addNode("H");		
		addRoadLink("BH");
		addRoadLink("HE");
		addRoadLink("EB");
		addRoadLink("FE");
		addRoadLink("FB");
		addRoadLink("CB");
		addRoadLink("FG");
		addRoadLink("GF");
		addRoadLink("GC");
		addRoadLink("CD");
		addRoadLink("DC");
		addRoadLink("AD");
		addRoadLink("AG");
		assertEquals(4, trajan.getNumComponents(nodes,roadLinks));
		List<Component> components = trajan.findComponents(nodes,roadLinks);
		assertEquals(4, components.size());
		
		List<Node> expected = new ArrayList<>();
		expected.add(nodeH);
		expected.add(nodeB);
		expected.add(nodeE);
		assertTrue(components.get(0).containsAll(expected));
		
		expected.clear();
		expected.add(nodeC);
		expected.add(nodeD);
		assertTrue(components.get(1).containsAll(expected));
		
		expected.clear();
		expected.add(nodeF);
		expected.add(nodeG);
		assertTrue(components.get(2).containsAll(expected));
		
		expected.clear();
		expected.add(nodeA);
		assertTrue(components.get(3).containsAll(expected));
	}
	
	@Test
	public void stronglyComponentTest() {
		addNode("A");
		addNode("B");
		addNode("C");
		addNode("D");
		addNode("E");
		addNode("F");
		addNode("G");
		addNode("H");
		addRoadLink("BA");
		addRoadLink("AE");
		addRoadLink("EB");
		addRoadLink("FE");
		addRoadLink("FB");
		addRoadLink("CB");
		addRoadLink("BC");
		addRoadLink("FG");
		addRoadLink("GF");
		addRoadLink("GC");
		addRoadLink("CD");
		addRoadLink("DC");
		addRoadLink("HD");
		addRoadLink("DH");
		addRoadLink("HG");
		assertEquals(1, trajan.getNumComponents(nodes,roadLinks));
		List<Component> components = trajan.findComponents(nodes,roadLinks);
		assertEquals(1, components.size());
		assertEquals(8, components.get(0).size());
		
	}

	private Node addNode(String nodeId) {
		Node node = new Node(nodeId);
		nodes.add(node);
		return node;
	}
	
	private RoadLink addRoadLink(String name) {
		RoadLink roadLink = createRoadLink(name);
		roadLinks.add(roadLink);
		return roadLink;
	}

	private RoadLink createRoadLink(String name) {
		String startId = "" + name.charAt(0);
		String endId = "" + name.charAt(1);
		RoadLink roadLink = new RoadLink(findNode(startId), findNode(endId),
				name, BigDecimal.ONE);
		return roadLink;
	}

	private Node findNode(String nodeId) {
		return nodes.get(nodes.indexOf(new Node(nodeId)));
	}
}
