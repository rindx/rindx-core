package osm.cleaner;

import static org.junit.Assert.*;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import osm.Bounds;
import osm.Node;
import osm.Tag;
import osm.Way;

public class DataCleanerTest {
	private static final String TEST_RESOURCES = "src/test/resources/osm/cleaner/";
	private static final String OUTPUT = "target/output/";
	private static final String TEST = "target/test/";
	private DataCleaner cleaner;
	private List<Node> nodes;
	private List<Way> ways;
	private Bounds bounds;

	@Before
	public void setUp() {
		mkDirs(OUTPUT);
		mkDirs(TEST);
		bounds = new Bounds("0", "0", "20", "20");
		cleaner = new DataCleaner(bounds, createWaysClassifier());
		nodes = new ArrayList<>();
		ways = new ArrayList<>();
	}

	private void mkDirs(String outputFile) {
		File file = new File(outputFile);
		file.mkdirs();
	}

	private WaysClassifier createWaysClassifier() {
		String usedTagsFile = TEST_RESOURCES + "used.txt";
		String usedIgnoreTagsFile = TEST_RESOURCES + "usedIgnore.txt";
		String compressTagsFile = TEST_RESOURCES + "compress.txt";
		WaysClassifier classifier = new WaysClassifier(usedTagsFile, usedIgnoreTagsFile, compressTagsFile);
		return classifier;
	}

	@After
	public void tearDown() {
		cleaner.clearCentroidIds();
	}

	@Test
	// Test against runtime errors
	public void runCleanTest() {
		cleaner.setSide(4.0);
		addFirstBuilding();
		addSeparateBuilding();
		addNorthHighway();
		addEastHighway();
		cleaner.clean(ways, nodes, "target/test/roadlinks.txt", "target/test/nodes.txt", "target/test/adj-list.txt");
	}

	@Test
	public void findNodeTest() {
		Node nodeA = new Node("A", "10", "20");
		nodes.add(nodeA);
		assertEquals(nodeA, DataCleaner.findNode("A", nodes));
		assertEquals(Node.EMPTY, DataCleaner.findNode("B", nodes));
	}

	@Test
	public void greaterThanMaxBoundsGridTest() {
		cleaner.setSide(1.0);

		nodes.add(new Node("1", "21", "21"));
		nodes.add(new Node("2", "22", "22"));

		int node1LatGridNo = cleaner.getLatGridNo(nodes.get(0));
		int node1LonGridNo = cleaner.getLonGridNo(nodes.get(0));

		assertEquals(20, node1LatGridNo);
		assertEquals(20, node1LonGridNo);

		int node2LatGridNo = cleaner.getLatGridNo(nodes.get(1));
		int node2LonGridNo = cleaner.getLonGridNo(nodes.get(1));

		assertEquals(20, node2LatGridNo);
		assertEquals(20, node2LonGridNo);
	}

	@Test
	public void getGridNoTest() {
		cleaner.setSide(1.0);

		nodes.add(new Node("1", "0.5", "0.5"));
		nodes.add(new Node("2", "1.0", "0.5"));
		nodes.add(new Node("3", "0.5", "1.0"));
		nodes.add(new Node("4", "1.0", "1.0"));

		int node1LatGridNo = cleaner.getLatGridNo(nodes.get(0));
		int node1LonGridNo = cleaner.getLonGridNo(nodes.get(0));

		assertEquals(0, node1LatGridNo);
		assertEquals(0, node1LonGridNo);

		int node2LatGridNo = cleaner.getLatGridNo(nodes.get(1));
		int node2LonGridNo = cleaner.getLonGridNo(nodes.get(1));

		assertEquals(1, node2LatGridNo);
		assertEquals(0, node2LonGridNo);

		int node3LatGridNo = cleaner.getLatGridNo(nodes.get(2));
		int node3LonGridNo = cleaner.getLonGridNo(nodes.get(2));

		assertEquals(0, node3LatGridNo);
		assertEquals(1, node3LonGridNo);

		int node4LatGridNo = cleaner.getLatGridNo(nodes.get(3));
		int node4LonGridNo = cleaner.getLonGridNo(nodes.get(3));

		assertEquals(1, node4LatGridNo);
		assertEquals(1, node4LonGridNo);
	}

	@Test
	public void addCentroidsTest() {
		addNorthHighway();
		addFirstBuilding();

		List<Node> centroids = cleaner.calculateAllCentroids(ways, nodes);
		cleaner.compressBuildings(ways, nodes, centroids);

		assertEquals(3, ways.get(0).getNdList().size());
		assertEquals(1, ways.get(1).getNdList().size());
	}

	@Test
	public void oneWayTest() {
		addNorthOnewayHighway();
		List<RoadLink> roadLinks = cleaner.extractAndBoxRoadLinks(ways, nodes);
		assertEquals(2, roadLinks.size());
	}

	@Test
	public void notOneWayTest() {
		addNorthHighway();
		List<RoadLink> roadLinks = cleaner.extractAndBoxRoadLinks(ways, nodes);
		assertEquals(4, roadLinks.size());
	}

	@Test
	public void purgeIsolatedNodesTest() {
		addNorthHighway();
		nodes.add(new Node("SUPER"));
		nodes.add(new Node("FLOUS"));
		List<RoadLink> roadLinks = cleaner.extractAndBoxRoadLinks(ways, nodes);
		nodes = DataCleaner.purgeIsolatedNodes(nodes, roadLinks);
		assertEquals(3, nodes.size());
	}

	@Test
	public void ignoreMissingRoadLinkNodeTest() {
		addNorthOnewayHighway();
		nodes.remove(0);
		List<RoadLink> roadLinks = cleaner.extractAndBoxRoadLinks(ways, nodes);
		assertEquals(1, roadLinks.size());
	}

	@Test
	public void connectCentroidsToNearestLinkTest() {
		cleaner.setSide(4.0);
		addNorthHighway();
		addEastHighway();

		List<Node> centroids = new ArrayList<>();
		ArrayList<Tag> tagA = new ArrayList<>();
		tagA.add(new Tag("name", "Alpha Building"));
		centroids.add(new Node("1", "2", "2", tagA));
		ArrayList<Tag> tagB = new ArrayList<>();
		tagB.add(new Tag("name", "Bravo Building"));
		centroids.add(new Node("5", "6", "2", tagB));

		List<RoadLink> roadLinks = cleaner.extractAndBoxRoadLinks(ways, nodes);
		List<RoadLink> buildingLinks = cleaner.connectCentroidsToNearestLink(roadLinks, centroids);

		assertEquals(4, buildingLinks.size());
	}

	@Test
	public void calculateCentroidTest() {
		addFirstBuildingNodes();
		Node centroid = cleaner.calculateCentroid(nodes);
		assertEquals("2.0000000", centroid.getLat());
		assertEquals("2.0000000", centroid.getLon());
	}

	@Test
	public void calculateCentroids() {
		addFirstBuilding();
		addSeparateBuilding();
		List<Node> centroids = cleaner.calculateAllCentroids(ways, nodes);
		assertEquals(2, centroids.size());
		checkNode(centroids.get(0), "1", "Alpha Building", "2.0000000", "2.0000000");
		checkNode(centroids.get(1), "5", "Bravo Building", "6.0000000", "2.0000000");
	}

	private void addFirstBuilding() {
		addFirstBuildingNodes();
		ways.add(createBuilding("1", "Alpha Building", new String[] { "1", "2", "3", "4" }));
	}

	private void addFirstBuildingNodes() {
		nodes.add(new Node("1", "3", "1"));
		nodes.add(new Node("2", "3", "3"));
		nodes.add(new Node("3", "1", "1"));
		nodes.add(new Node("4", "1", "3"));
	}

	private Way createBuilding(String id, String name, String... nodeIds) {
		Way building = new Way(id, nodeIds);
		ArrayList<Tag> localTags = new ArrayList<>();
		localTags.add(new Tag("building", "yes"));
		localTags.add(new Tag("name", name));
		building.setTagList(localTags);
		return building;
	}

	private void addSeparateBuilding() {
		addSeparateBuildingNodes();
		ways.add(createBuilding("2", "Bravo Building", new String[] { "5", "6", "7", "8" }));
	}

	private void addSeparateBuildingNodes() {
		nodes.add(new Node("5", "7", "1"));
		nodes.add(new Node("6", "7", "3"));
		nodes.add(new Node("7", "5", "1"));
		nodes.add(new Node("8", "5", "3"));
	}

	private void checkNode(Node node, String id, String name, String lat, String lon) {
		assertEquals(id, node.getId());
		assertEquals(name, node.getName());
		assertEquals(lat, node.getLat());
		assertEquals(lon, node.getLon());
	}

	@Test
	public void calculateCentroidsOfIntersectingBuildings() {
		addFirstBuilding();
		addIntersectingBuilding();
		List<Node> centroids = cleaner.calculateAllCentroids(ways, nodes);
		assertEquals(2, centroids.size());
		checkNode(centroids.get(0), "1", "Alpha Building", "2.0000000", "2.0000000");
		checkNode(centroids.get(1), "2", "Charlie Building", "4.0000000", "2.0000000");
	}

	private void addIntersectingBuilding() {
		addSeparateBuildingNodes();
		ways.add(createBuilding("3", "Charlie Building", new String[] { "1", "2", "8", "7" }));
	}

	@Test
	public void deleteBuildingNodesTest() {
		addFirstBuilding();
		cleaner.deleteBuildingNodes(ways, nodes);
		assertTrue(nodes.isEmpty());
		for (Way w : ways) {
			assertTrue(w.getNdList().isEmpty());
		}
	}

	@Test
	public void compressBuildingsTest() {
		addFirstBuilding();
		List<Node> centroids = cleaner.calculateAllCentroids(ways, nodes);
		List<Way> compressed = cleaner.compressBuildings(ways, nodes, centroids);

		assertEquals(1, compressed.get(0).getNdList().size());
		assertEquals("1", compressed.get(0).getNdList().get(0).getRef());
		assertEquals(4, nodes.size());
		checkNode(nodes.get(0), "1", "Alpha Building", "2.0000000", "2.0000000");
	}

	@Test
	public void extractRoadLinkTest() {
		addNorthHighway();

		List<RoadLink> roadLinks = cleaner.extractAndBoxRoadLinks(ways, nodes);

		assertEquals(4, roadLinks.size());
		RoadLink rl1 = roadLinks.get(0);
		assertEquals("11", rl1.getStartNode().getId());
		assertEquals("12", rl1.getEndNode().getId());
		assertEquals("North Highway", rl1.getName());
		assertEquals(new BigDecimal("110.1128"), rl1.getDistance());
	}

	private void addNorthHighway() {
		addNorthHighwayNodes();
		ways.add(createHighway("4", "North Highway", new String[] { "11", "12", "13" }));
	}

	private void addNorthOnewayHighway() {
		addNorthHighwayNodes();
		ways.add(createOnewayHighway("4", "North Highway", new String[] { "11", "12", "13" }));
	}

	private void addNorthHighwayNodes() {
		nodes.add(new Node("11", "8", "1"));
		nodes.add(new Node("12", "8", "2"));
		nodes.add(new Node("13", "8", "3"));
	}

	private void addEastHighway() {
		addEastHighwayNodes();
		ways.add(createHighway("5", "East Highway", new String[] { "13", "14", "9", "10" }));
	}

	private void addEastHighwayNodes() {
		if (!nodes.contains(new Node("13", "8", "3"))) {
			nodes.add(new Node("13", "8", "3"));
		}
		nodes.add(new Node("14", "6", "6"));
		nodes.add(new Node("9", "3", "6"));
		nodes.add(new Node("10", "1", "6"));
	}

	private Way createHighway(String id, String name, String... nodeIds) {
		Way highway = new Way(id, nodeIds);
		ArrayList<Tag> localTags = new ArrayList<>();
		localTags.add(new Tag("highway", "primary"));
		localTags.add(new Tag("name", name));
		highway.setTagList(localTags);
		return highway;
	}

	private Way createOnewayHighway(String id, String name, String... nodeIds) {
		Way highway = new Way(id, nodeIds);
		ArrayList<Tag> localTags = new ArrayList<>();
		localTags.add(new Tag("highway", "primary"));
		localTags.add(new Tag("oneway", "yes"));
		localTags.add(new Tag("name", name));
		highway.setTagList(localTags);
		return highway;
	}
}
