package osm.cleaner

import osm.Node
import spock.lang.Specification

class AdjacencyListFormatterSpec extends Specification {
    List<Node> nodes = []
    List<RoadLink> roadLinks = []

    def 'Should format to adjacency list'() {
        setup:
        addNodes("A", "B", "C")
        addRoadLink("AB")
        addRoadLink("BC")
        addRoadLink("CA")
        addRoadLink("AC")

        expect:
        AdjacencyListFormatter.formatAdjString(nodes, roadLinks) == "A;B,C\nB;C\nC;A"
    }

    def 'No outgoing edges first node'() {
        setup:
        addNodes("A", "B", "C", "D")
        addRoadLink("BA")
        addRoadLink("BC")
        addRoadLink("CA")
        addRoadLink("CB")
        addRoadLink("DB")
        addRoadLink("DC")

        expect:
        AdjacencyListFormatter.formatAdjString(nodes, roadLinks) == "A;\nB;A,C\nC;A,B\nD;B,C"
    }

    def 'No outgoing edges middle node'() {
        setup:
        addNodes("A", "B", "C", "D")
        addRoadLink("AC")
        addRoadLink("AD")
        addRoadLink("CB")
        addRoadLink("CD")
        addRoadLink("DB")
        addRoadLink("DC")

        expect:
        AdjacencyListFormatter.formatAdjString(nodes, roadLinks) == "A;C,D\nB;\nC;B,D\nD;B,C"
    }

    def 'No outgoing edges final node'() {
        setup:
        addNodes("A", "B", "C", "D")
        addRoadLink("AB")
        addRoadLink("AC")
        addRoadLink("BC")
        addRoadLink("BD")
        addRoadLink("CB")
        addRoadLink("CD")

        expect:
        AdjacencyListFormatter.formatAdjString(nodes, roadLinks) == "A;B,C\nB;C,D\nC;B,D\nD;"
    }

    private void addNodes(String... ids) {
        for (String id in ids) {
            nodes.add(new Node(id))
        }
    }

    private void addRoadLink(String roadName) {
        Node nodeFrom = findNode("${roadName.charAt(0)}")
        Node nodeTo = findNode("${roadName.charAt(1)}")
        addRoadLink(nodeFrom, nodeTo, roadName, 1)
    }

    private Node findNode(String nodeFromId) {
        nodes.get(nodes.indexOf(new Node(nodeFromId)))
    }

    private void addRoadLink(Node nodeFrom, Node nodeTo, String roadName, BigDecimal cost) {
        RoadLink roadLink = new RoadLink(nodeFrom, nodeTo, roadName, cost)
        roadLinks << roadLink
    }
}
