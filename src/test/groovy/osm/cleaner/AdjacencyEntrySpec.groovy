package osm.cleaner

import osm.Node
import spock.lang.Specification

class AdjacencyEntrySpec extends Specification {

    def 'Should add node'() {
        setup:
        Node node = new Node("A", "1.0", "2.0")
        AdjacencyEntry entry = new AdjacencyEntry(node)

        when:
        entry.addNode("B", "0.005", "AB")

        then:
        entry.toString() == '{"id":"A","loc":"[2.0,1.0]","name":"","adjacent_nodes":[{"id":"B","distance":"0.005","roadlink":"AB"}]}'

        when:
        entry.addNode("C", "0.002", "AC")

        then:
        entry.toString() == '{"id":"A","loc":"[2.0,1.0]","name":"","adjacent_nodes":[{"id":"B","distance":"0.005","roadlink":"AB"},' +
                '{"id":"C","distance":"0.002","roadlink":"AC"}]}'
    }
}
