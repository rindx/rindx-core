package osm.cleaner;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import osm.Node;

public class JsonFormatterTest {
	private List<Node> nodes;
	private List<RoadLink> roadLinks;

	@Before
	public void setUp() {
		nodes = new ArrayList<>();
		roadLinks = new ArrayList<>();
	}

	@Test
	public void quotesTest() {
		addNode("A", "1.0", "2.0");
		addNode("B", "1.005", "2.0");
		addNode("C", "1.0", "2.002");
		addRoadLink("A", "B", "Randy \"Red Blood\" Revolver", "0.005");
		addRoadLink("A", "C", "Manny \'Pacman\' Pacquiao", "0.002");
		String adjListContents = "A;B,C\nB;\nC;\n";
		String output = JsonFormatter.format(nodes, roadLinks, adjListContents);
		String aAdj = formatToJSON("B", "0.005",
				"Randy \\\"Red Blood\\\" Revolver")
				+ ","
				+ formatToJSON("C", "0.002", "Manny \\\'Pacman\\\' Pacquiao");
		String expected = formatToJSON("A", "[2.0,1.0]", "", aAdj) + "\n"
				+ formatToJSON("B", "[2.0,1.005]", "", "") + "\n"
				+ formatToJSON("C", "[2.002,1.0]", "", "");
		assertEquals(expected, output);
	}

	@Test
	public void simpleFormatTest() {
		addNode("A", "1.0", "2.0");
		addNode("B", "1.005", "2.0");
		addNode("C", "1.0", "2.002");
		addRoadLink("AB", "0.005");
		addRoadLink("AC", "0.002");
		String adjListContents = "A;B,C\nB;\nC;\n";
		String output = JsonFormatter.format(nodes, roadLinks, adjListContents);
		String aAdj = formatToJSON("B", "0.005", "AB") + ","
				+ formatToJSON("C", "0.002", "AC");
		String expected = formatToJSON("A", "[2.0,1.0]", "", aAdj) + "\n"
				+ formatToJSON("B", "[2.0,1.005]", "", "") + "\n"
				+ formatToJSON("C", "[2.002,1.0]", "", "");
		assertEquals(expected, output);
	}

	@Test
	public void formatWithGapsMiddleTest() {
		addNode("A", "lat", "lon");
		addNode("B", "lat", "lon");
		addNode("C", "lat", "lon");
		addNode("D", "lat", "lon");
		addRoadLink("AC", "0.005");
		addRoadLink("AD", "0.005");
		addRoadLink("CB", "0.005");
		addRoadLink("CD", "0.005");
		addRoadLink("DB", "0.005");
		addRoadLink("DC", "0.005");
		String adjListContents = AdjacencyListFormatter.formatAdjString(nodes,
				roadLinks);
		String output = JsonFormatter.format(nodes, roadLinks, adjListContents);
		String aAdj = formatToJSON("C", "0.005", "AC") + ","
				+ formatToJSON("D", "0.005", "AD");
		String cAdj = formatToJSON("B", "0.005", "CB") + ","
				+ formatToJSON("D", "0.005", "CD");
		String dAdj = formatToJSON("B", "0.005", "DB") + ","
				+ formatToJSON("C", "0.005", "DC");
		String expected = formatToJSON("A", "[lon,lat]", "", aAdj) + "\n"
				+ formatToJSON("B", "[lon,lat]", "", "") + "\n"
				+ formatToJSON("C", "[lon,lat]", "", cAdj) + "\n"
				+ formatToJSON("D", "[lon,lat]", "", dAdj);
		assertEquals(expected, output);
	}

	private String formatToJSON(String id, String loc, String name,
			String adjacentNodes) {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append("\"id\":\"").append(id).append("\",");
		builder.append("\"loc\":\"").append(loc).append("\",");
		builder.append("\"name\":\"").append(name).append("\",");
		builder.append("\"adjacent_nodes\":[").append(adjacentNodes)
				.append("]");
		builder.append("}");
		return builder.toString();
	}

	private String formatToJSON(String id, String distance, String roadLink) {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append("\"id\":\"").append(id).append("\",");
		builder.append("\"distance\":\"").append(distance).append("\",");
		builder.append("\"roadlink\":\"").append(roadLink).append("\"");
		builder.append("}");
		return builder.toString();
	}

	private Node addNode(String id, String lat, String lon) {
		Node nodeA = new Node(id, lat, lon);
		nodes.add(nodeA);
		return nodeA;
	}

	private RoadLink addRoadLink(String roadName, String cost) {
		String nodeFromId = "" + roadName.charAt(0);
		String nodeToId = "" + roadName.charAt(1);
		return addRoadLink(nodeFromId, nodeToId, roadName, cost);
	}

	private Node findNode(String nodeFromId) {
		return nodes.get(nodes.indexOf(new Node(nodeFromId)));
	}

	private RoadLink addRoadLink(String nodeFromId, String nodeToId,
			String roadName, String cost) {
		Node nodeFrom = findNode(nodeFromId);
		Node nodeTo = findNode(nodeToId);
		return addRoadLink(nodeFrom, nodeTo, roadName, new BigDecimal(cost));
	}

	private RoadLink addRoadLink(Node nodeFrom, Node nodeTo, String roadName,
			BigDecimal cost) {
		RoadLink roadLink = new RoadLink(nodeFrom, nodeTo, roadName, cost);
		roadLinks.add(roadLink);
		return roadLink;
	}
}
