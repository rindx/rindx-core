package osm.command

import spock.lang.Specification

import static osm.TestUtils.deleteFileInTarget
import static osm.TestUtils.resource
import static osm.TestUtils.target

class SelectRandomNodeIdsCommandSpec extends Specification {
    def 'Should create file containing randomly selected nodes'() {
        setup:
        deleteFileInTarget("nodeIds.csv")
        String inputNodesFilename = resource("nodes.txt")
        int numToSelect = 5
        String outputNodeIdsFilename = target("nodeIds.csv")
        File actualOutput = new File(outputNodeIdsFilename)
        SelectRandomNodeIdsCommand command = new SelectRandomNodeIdsCommand(
                inputNodesFilename, numToSelect, outputNodeIdsFilename)

        when:
        command.execute()

        then:
        actualOutput.exists()
    }
}
