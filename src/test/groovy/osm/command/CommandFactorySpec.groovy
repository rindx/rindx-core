package osm.command

import spock.lang.Specification

import static CommandFactory.CLEAN_NB
import static CommandFactory.CLEAN_WB
import static CommandFactory.GET_DEGREES
import static CommandFactory.REPAIRED_ROAD_GENERAL_ANALYSIS
import static CommandFactory.REPAIRED_ROAD_LEVEL_THREE_ANALYSIS
import static CommandFactory.SELECT_RANDOM_NODE_IDS
import static osm.TestUtils.CLEANER_CONFIG
import static osm.TestUtils.resource

class CommandFactorySpec extends Specification {
    static final String TEST_OSM = resource('test-osm.osm')
    CommandFactory commandFactory = new CommandFactory()
    String command

    def 'Should create GetDegreesCommand'() {
        setup:
        command = GET_DEGREES
        String inputNodesFilename = 'nodes-02.txt'
        String inputRoadlinksFilename = 'roadlinks-02.txt'
        String outputDegreesFilename = 'degrees.csv'

        when:
        GetDegreesCommand actualCommand = createCommand(command, inputNodesFilename,
                inputRoadlinksFilename, outputDegreesFilename) as GetDegreesCommand

        then:
        actualCommand.inputRoadlinksFilename == inputRoadlinksFilename
        actualCommand.inputNodesFilename == inputNodesFilename
        actualCommand.outputDegreesFilename == outputDegreesFilename
    }

    def 'Should create SelectRandomNodeIdsCommand'() {
        setup:
        command = SELECT_RANDOM_NODE_IDS
        String inputNodesFilename = 'nodes.txt'
        String numToSelect = '5'
        String outputNodeIdsFilename = 'nodeIds.csv'

        when:
        SelectRandomNodeIdsCommand actualCommand = createCommand(command, inputNodesFilename,
                numToSelect, outputNodeIdsFilename) as SelectRandomNodeIdsCommand

        then:
        actualCommand.inputNodesFilename == inputNodesFilename
        actualCommand.numToSelect == Integer.parseInt(numToSelect)
        actualCommand.outputNodeIdsFilename == outputNodeIdsFilename
    }

    def 'Should create RepairedRoadGeneralAnalysisCommand'() {
        setup:
        command = REPAIRED_ROAD_GENERAL_ANALYSIS
        String repairedRoadsFilename = 'repair.txt'
        String outputFilename = 'repair-output-G.txt'

        when:
        RepairedRoadGeneralAnalysisCommand actualCommand = createCommand(command, repairedRoadsFilename,
                outputFilename) as RepairedRoadGeneralAnalysisCommand

        then:
        actualCommand.repairedRoadsFilename == repairedRoadsFilename
        actualCommand.outputFilename == outputFilename
    }

    def 'Should create RepairedRoadLevelThreeAnalysisCommand'() {
        setup:
        command = REPAIRED_ROAD_LEVEL_THREE_ANALYSIS
        String repairedRoadsFilename = 'repair.txt'
        String outputFilename = 'repair-output-LVL3.txt'

        when:
        RepairedRoadLevelThreeAnalysisCommand actualCommand = createCommand(command, repairedRoadsFilename,
                outputFilename) as RepairedRoadLevelThreeAnalysisCommand

        then:
        actualCommand.repairedRoadsFilename == repairedRoadsFilename
        actualCommand.outputFilename == outputFilename
    }

    def 'Should create CleanCommands with specified Config folder'() {
        when:
        def actualCommand = createCommand(command, inputOsm, outputNodes,
                outputRoadlinks, outputAdjList, configFolder)

        then:
        actualCommand.class == type
        actualCommand.osmFilename == inputOsm
        actualCommand.nodesFilename == outputNodes
        actualCommand.roadLinksFilename == outputRoadlinks
        actualCommand.adjListFilename == outputAdjList
        actualCommand.configFolder == configFolder

        where:
        command  | type                      | inputOsm | outputNodes    | outputRoadlinks    | outputAdjList     | configFolder
        CLEAN_NB | CleanNoBuildingsCommand   | TEST_OSM | 'nodes-nb.txt' | 'roadlinks-nb.txt' | 'adj-list-nb.txt' | CLEANER_CONFIG
        CLEAN_WB | CleanWithBuildingsCommand | TEST_OSM | 'nodes.txt'    | 'roadlinks.txt'    | 'adj-list.txt'    | CLEANER_CONFIG
    }

    def 'Should create CleanCommands with default Config folder'() {
        when:
        def actualCommand = createCommand(command, inputOsm, outputNodes,
                outputRoadlinks, outputAdjList)

        then:
        actualCommand.class == type
        actualCommand.osmFilename == inputOsm
        actualCommand.nodesFilename == outputNodes
        actualCommand.roadLinksFilename == outputRoadlinks
        actualCommand.adjListFilename == outputAdjList
        actualCommand.configFolder == configFolder

        where:
        command  | type                      | inputOsm | outputNodes    | outputRoadlinks    | outputAdjList     | configFolder
        CLEAN_NB | CleanNoBuildingsCommand   | TEST_OSM | 'nodes-nb.txt' | 'roadlinks-nb.txt' | 'adj-list-nb.txt' | CleanNoBuildingsCommand.DEFAULT_CONFIG_FOLDER
        CLEAN_WB | CleanWithBuildingsCommand | TEST_OSM | 'nodes.txt'    | 'roadlinks.txt'    | 'adj-list.txt'    | CleanWithBuildingsCommand.DEFAULT_CONFIG_FOLDER
    }

    private Command createCommand(String command, String... args) {
        String[] argsWithCommand = new String[args.length + 1]
        argsWithCommand[0] = command
        for (int i = 0; i < args.length; i++) {
            argsWithCommand[i + 1] = args[i]
        }
        return commandFactory.createCommand(command, argsWithCommand)
    }
}
