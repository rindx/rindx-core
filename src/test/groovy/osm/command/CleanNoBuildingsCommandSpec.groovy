package osm.command

import spock.lang.Specification

import static osm.TestUtils.CLEANER_CONFIG
import static osm.TestUtils.assertFileEquals
import static osm.TestUtils.deleteFileInTarget
import static osm.TestUtils.resource
import static osm.TestUtils.target

class CleanNoBuildingsCommandSpec extends Specification {
    def setup() {
        deleteFileInTarget('nodes-nb.txt')
        deleteFileInTarget('roadlinks-nb.txt')
        deleteFileInTarget('adj-list-nb.txt')
    }

    def 'Should make filtered output files from osm'() {
        setup:
        String inputOsm = resource('test-osm.osm')
        String expectedNodes = resource('nodes-nb-expected.csv')
        String outputNodes = target('nodes-nb.txt')
        String expectedRoadlinks = resource('roadlinks-nb-expected.csv')
        String outputRoadlinks = target('roadlinks-nb.txt')
        String expectedAdjList = resource('adj-list-nb-expected.csv')
        String outputAdjList = target('adj-list-nb.txt')
        CleanNoBuildingsCommand command = new CleanNoBuildingsCommand(inputOsm, outputNodes,
                outputRoadlinks, outputAdjList, CLEANER_CONFIG)

        when:
        command.execute()

        then:
        assertFileEquals(expectedNodes, outputNodes)
        assertFileEquals(expectedRoadlinks, outputRoadlinks)
        assertFileEquals(expectedAdjList, outputAdjList)
    }

    def 'Should use default Config folder when not specified'() {
        setup:
        String inputOsm = resource('test-osm.osm')
        String outputNodes = target('nodes-nb.txt')
        String outputRoadlinks = target('roadlinks-nb.txt')
        String outputAdjList = target('adj-list-nb.txt')

        when:
        CleanNoBuildingsCommand command = new CleanNoBuildingsCommand(inputOsm, outputNodes,
                outputRoadlinks, outputAdjList)

        then:
        command.configFolder == ''
    }
}