package osm.command

import spock.lang.Specification

import static osm.TestUtils.assertFileEquals
import static osm.TestUtils.resource
import static osm.TestUtils.target

class GetDegreesCommandSpec extends Specification {
    def 'Should create file that shows node degrees'() {
        setup:
        String inputNodesFilename = resource("nodes-02.txt")
        String inputRoadlinksFilename = resource("roadlinks-02.txt")
        String outputDegreesFilename = target("degrees.csv")
        String expectedOutput = resource("degrees-01.csv")
        GetDegreesCommand actualCommand = new GetDegreesCommand(inputNodesFilename, inputRoadlinksFilename,
                outputDegreesFilename)

        when:
        actualCommand.execute()

        then:
        assertFileEquals(expectedOutput, outputDegreesFilename)
    }

}