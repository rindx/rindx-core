package osm.command

import spock.lang.Specification

import static osm.TestUtils.assertFileEquals
import static osm.TestUtils.deleteFileInTarget
import static osm.TestUtils.resource
import static osm.TestUtils.target

class RepairedRoadLevelThreeAnalysisCommandSpec extends Specification {
    static final String REPAIR_OUTPUT_LVL3 = "repair-output-LVL3.txt"

    def'Should create file that analyzes repaired roads up to level 3'() {
        setup:
        deleteFileInTarget(REPAIR_OUTPUT_LVL3)
        String repairedRoadsFilename = resource("repair.txt")
        String outputFilename = target(REPAIR_OUTPUT_LVL3)
        String expectedOutput = resource(REPAIR_OUTPUT_LVL3)
        Command command = new RepairedRoadLevelThreeAnalysisCommand(repairedRoadsFilename, outputFilename)

        when:
        command.execute()

        then:
        assertFileEquals(expectedOutput, outputFilename)
    }
}
