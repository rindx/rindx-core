package osm.command

import spock.lang.Specification

import static osm.TestUtils.CLEANER_CONFIG
import static osm.TestUtils.assertFileEquals
import static osm.TestUtils.deleteFileInTarget
import static osm.TestUtils.resource
import static osm.TestUtils.target

class CleanWithBuildingsCommandSpec extends Specification {
    def setup() {
        deleteFileInTarget("nodes.txt")
        deleteFileInTarget("roadlinks.txt")
        deleteFileInTarget("adj-list.txt")
    }

    def 'Should make filtered output files from osm'() {
        setup:
        String inputOsm = resource("test-osm.osm")
        String expectedNodes = resource("nodes-expected.csv")
        String outputNodes = target("nodes.txt")
        String expectedRoadlinks = resource("roadlinks-expected.csv")
        String outputRoadlinks = target("roadlinks.txt")
        String expectedAdjList = resource("adj-list-expected.csv")
        String outputAdjList = target("adj-list.txt")
        CleanWithBuildingsCommand command = new CleanWithBuildingsCommand(inputOsm, outputNodes,
                outputRoadlinks, outputAdjList, CLEANER_CONFIG)

        when:
        command.execute()

        then:
        assertFileEquals(expectedNodes, outputNodes)
        assertFileEquals(expectedRoadlinks, outputRoadlinks)
        assertFileEquals(expectedAdjList, outputAdjList)
    }

    def 'Should use default Config folder when not specified'() {
        setup:
        String inputOsm = resource("test-osm.osm")
        String outputNodes = target("nodes.txt")
        String outputRoadlinks = target("roadlinks.txt")
        String outputAdjList = target("adj-list.txt")

        when:
        CleanWithBuildingsCommand command = new CleanWithBuildingsCommand(inputOsm, outputNodes,
                outputRoadlinks, outputAdjList)

        then:
        command.configFolder == ''
    }
}