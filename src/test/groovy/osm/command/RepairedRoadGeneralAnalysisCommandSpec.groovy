package osm.command

import spock.lang.Specification

import static osm.TestUtils.assertFileEquals
import static osm.TestUtils.deleteFileInTarget
import static osm.TestUtils.resource
import static osm.TestUtils.target

class RepairedRoadGeneralAnalysisCommandSpec extends Specification {
    def 'Should create file that analyzes repaired roads'() {
        setup:
        deleteFileInTarget("repair-output.txt")
        String repairedRoadsFilename = resource("repair.txt")
        String outputFilename = target("repair-output-G.txt")
        String expectedOutput = resource("repair-output-G.txt")
        Command command = new RepairedRoadGeneralAnalysisCommand(repairedRoadsFilename, outputFilename)

        when:
        command.execute()

        then:
        assertFileEquals(expectedOutput, outputFilename)
    }
}
