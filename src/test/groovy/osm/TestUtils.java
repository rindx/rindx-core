package osm;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import osm.cleaner.RoadLink;

public class TestUtils {
	public static final String TEST_RESOURCES = "src/test/resources/osm/";
	public static final String CLEANER_CONFIG = TEST_RESOURCES + "cleaner/";
	public static final String TARGET = "target/test/";

	public static String resource(String filename) {
		return TEST_RESOURCES + filename;
	}

	public static String target(String filename) {
		return TARGET + filename;
	}

	public static void assertFileEquals(String expected, String actual)
			throws FileNotFoundException {
		try (Scanner scanner = new Scanner(new File(actual))) {
			try (Scanner expectedScanner = new Scanner(new File(expected))) {
				while (expectedScanner.hasNext()) {
					assertEquals(expectedScanner.nextLine(), scanner.nextLine());
				}
			}
		}
	}

	public static void assertNodes(List<Node> expected, List<Node> actual) {
		for (int i = 0; i < expected.size(); i++) {
			assertNode(expected.get(i), actual.get(i));
		}
	}

	public static void assertNode(Node expected, Node actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getLat(), actual.getLat());
		assertEquals(expected.getLon(), actual.getLon());
		assertEquals(expected.getName(), actual.getName());
	}
	
	public static void assertRoadlinks(List<RoadLink> expected, List<RoadLink> actual) {
		for (int i = 0; i < expected.size(); i++) {
			assertRoadlink(expected.get(i), actual.get(i));
		}
	}
	
	public static void assertRoadlink(RoadLink expected, RoadLink actual){
		assertEquals(expected.getStartNode(), actual.getStartNode());
		assertEquals(expected.getEndNode(), actual.getEndNode());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getDistance(), actual.getDistance());
		assertEquals(expected.getLanes(), actual.getLanes());
		assertEquals(expected.getHighwayType(), actual.getHighwayType());
	}
	
	public static void deleteFileInTarget(String filename) {
		File file = new File(target(filename));
		file.delete();
	}
}
